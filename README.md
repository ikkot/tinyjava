# TinyJava


## Ejecucion
Los archivos para ejecutar se encuentran en la carpeta  ArchivosFinales

### Diagrama de clases
![alt text](tinyjava/img/TinyJavaSintactico.png)

### Automata finito 
![alt text](tinyjava/img/Automata.png)

Block --> Bloque 
Asig --> Asignacion
Call --> Llamada
Return --> Retorno
BinEx --> Exprecion binaria
-En- --> Encadenado
-Ex- --> Exprecion
-R- --> Nodo derecho
-L- --> Nodo izquierdo
-C- --> Condicion
-S- --> Sentencia
-E- --> Else
