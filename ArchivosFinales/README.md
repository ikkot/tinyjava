# TinyJava



## Archivo Final
### Para ejecutar .jar

- Para ejecutar imprimiendo en pantalla : `java -cp Tinyjava.jar Main prueba.tj`

### Para ejecutar .class

- Descomprima el rar correspondiente Tinyjava.class.rar
- Para ejecutar imprimiendo en pantalla : `java Main prueba.tj`


## Analizador Lexico
### Para ejecutar .jar

- Para ejecutar con interfaz : `java -cp TinyJavaLex.jar Main`
- Para ejecutar imprimiendo en pantalla : `java -cp TinyJavaLex.jar Main prueba.tj`
- Para ejecutar escribiendo archivo : `java -cp TinyJavaLex.jar Main prueba.tj salida.txt`

### Para ejecutar .class

- Descomprima el rar correspondiente TintjavaLex.class.rar
- Para ejecutar con interfaz : `java Main`
- Para ejecutar imprimiendo en pantalla : `java Main prueba.tj`
- Para ejecutar escribiendo archivo : `java Main prueba.tj salida.txt`


## Analizador Sintactico
### Para ejecutar .jar

- Para ejecutar imprimiendo en pantalla : `java -cp TinyJavaS.jar Main prueba.tj`

### Para ejecutar .class

- Descomprima el rar correspondiente TintjavaS.class.rar
- Para ejecutar imprimiendo en pantalla : `java -cp TinyJavaS.jar Main prueba.tj`

## Analizador Semantico
### Para ejecutar .jar

- Para ejecutar imprimiendo en pantalla : `java -cp TinyjavaSemantic.jar Main prueba.tj`

### Para ejecutar .class

- Descomprima el rar correspondiente TinyjavaSemantic.class.rar
- Para ejecutar imprimiendo en pantalla : `java -cp TinyJavaS.jar Main prueba.tj`

