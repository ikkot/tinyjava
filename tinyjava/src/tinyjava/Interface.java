package tinyjava;

import java.util.Scanner;
import java.io.File;
import java.util.HashMap;

/**
 * Se encarga de crear una sensilla Interface por consola para cargar archivos y
 * ejecutar el lexicalAnalyzer
 */
public class Interface {

    private static Interface inter;/**< Interface */
    private int count = 0; /**< Contador */
    private Scanner imputScanner = new Scanner(System.in); /**< Declaro Scanner */
    private String inputTecladoString = ""; /**< Ingreso por consola */
    private String path; /**< Dirección del archivo a buscar archivos validos . */
    private String finalPath; /**< Dirección del archivo final seleccionado */
    private HashMap<Integer, String> fileName; /**< Archivos validos encontrados asociados a un numero */

    /**
     * Crea una instancia de la Interface de modo que sea un objeto singleton
     * @return Interface .
     */
    public static Interface getInstance(String path) {
        if (inter == null) {
            inter = new Interface(path);
        }
        return inter;
    }

    private Interface(String path) {
        this.path = path;
    }


    /**
     * Se encarga en primera instancia de buscar un archivo valido ubicado en el
     * path y en caso de encontrar los imprime en pantalla, para luego llamar a
     * fileOk() para obtener una ruta de un archivo seleccionado. En caso de no
     * encontrar ningun archivo valido (termina en .tj), solicita una nueva ruta
     * donde buscar o termina la ejecucion
     * 
     * @return finalPath .
     */
    public String searchFile() {
        count = 0;
        fileName = new HashMap<Integer, String>() {
        };
        System.out.println("");

        try {
            System.out.println("Archivos validos para ejecutar:");
            final File carpeta = new File(path);
            for (final File ficheroEntrada : carpeta.listFiles()) {
                if (ficheroEntrada.isDirectory()) {
                    System.out.println(" --"+ficheroEntrada.getName());
                    final File carpeta1 = new File(path+"/"+ficheroEntrada.getName());
                    for (final File ficheroEntrada2 : carpeta1.listFiles()) {
                        if (ficheroEntrada2.getName().contains(".tj")){
                            String[] nam1 = ficheroEntrada2.getName().split(".tj");
                            System.out.println("     " + count + ") " + nam1[0]);
                            fileName.put(count, ficheroEntrada.getName()+"/"+nam1[0]);
                            count++;
                        }
                    }


                } else {
                    if (ficheroEntrada.getName().contains(".tj")){
                        String[] nam = ficheroEntrada.getName().split(".tj");
                        System.out.println("     " + count + ") " + nam[0]);
                        fileName.put(count, nam[0]);
                        count++;
                    }
                }
            }
            if (fileOK()) {
                return finalPath;
            } else {
                return searchFile();
            }

        } catch (Exception e) {
            System.out.print(
                    "No se encontro archivos validos en la carpeta test. Quiere ingresar un nuevo path absoluto de la carpeta (Y/n)");
            inputTecladoString = imputScanner.nextLine();
            if (inputTecladoString.equals("n")) {
                return searchFile();
            } else {
                System.out.print("Ingres path absoluto de la carpeta: ");
                inputTecladoString = imputScanner.nextLine();
                this.path = inputTecladoString;

                return searchFile();
            }
        }
    }

    /**
     * Se encarga de ingresar una nueva ruta en donde buscar archivos, o ingresar un
     * numero correspondiente a uno de los archivos encontrados para asi obtener su
     * ruta correspondiente
     * 
     * @return Boolean False si no encontro una ruta final, True si encuntra una ruta a un archivo final .
     */
    private Boolean fileOK() {
        System.out.print("Si desea cargar una nueva ruta absoluta de carpeta precione 'p', sino ingrese numero correspondiente a uno de los archivo encontrados: ");
        inputTecladoString = imputScanner.nextLine();

        try {
            if (inputTecladoString.equals("p") || (count > Integer.parseInt(inputTecladoString))) {
                if (inputTecladoString.equals("p")) {
                    System.out.print("Ingres path absoluto de la carpeta: ");
                    inputTecladoString = imputScanner.nextLine();
                    this.path = inputTecladoString;
                    return false;
                } else {
                    String nameFileFinal = fileName.get(Integer.parseInt(inputTecladoString));
                    finalPath = path + "/" + nameFileFinal + ".tj";
                    return true;
                }
            } else {
                System.out.println("--------------------------------------------------------");
                System.out.println("Seleccione una opcion valida");
                return false;
            }

        } catch (Exception e) {
            System.out.println("--------------------------------------------------------");
            System.out.println("Seleccione una opcion valida");
            return false;
        }
    }

}
