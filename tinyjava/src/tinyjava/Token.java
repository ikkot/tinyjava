package tinyjava;

/**
 * Token representa, como dice el nombre, un token léxico.
 * Cada Token tiene una propiedad donde almacenar el lexicon, además del tipo de token que representa.
 * Por ejemplo, un identificador i será almacenado como: Token("i","identificador") 
 */
public class Token {
	private String lexicon; /**<Lexicon del token encontrado.*/
	private String token; /**<Tipo de token encontrado. */
	private int lineNum; /**Línea donde se encontró el token. */
	private int colNum; /**Columna donde se encontró el token. */
	
	public Token()
	{
	
	}

	public Token(String lexicon,String token,int line, int colum)
	{
		this.lexicon = lexicon;
		this.token = token;
		this.lineNum = line;
		this.colNum = colum;
	}

	public String getLexicon() {
		return lexicon;
	}

	public void setLexicon(String lexicon) {
		this.lexicon = lexicon;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getLineNum() {
		return lineNum;
	}

	public void setLineNum(int lineNum) {
		this.lineNum = lineNum;
	}

	public int getColNum() {
		return colNum;
	}

	public void setColNum(int colNum) {
		this.colNum = colNum;
	}

}
