package tinyjava.AnalizadorSintactico;

import java.util.LinkedList;
import java.util.Optional;
import java.util.ArrayList;
import tinyjava.Token;
import tinyjava.AnalizadorLexico.*;
import tinyjava.AnalizadorSemantico.SemanticAnalyzer;
import tinyjava.AnalizadorSemantico.TablaSimbolos.*;
import tinyjava.AnalizadorSemantico.Types.*;
import tinyjava.AnalizadorSemantico.Exceptions.*;
import tinyjava.AnalizadorSemantico.AST.ASTNode;
import tinyjava.AnalizadorSemantico.AST.Encadenado.*;
import tinyjava.AnalizadorSemantico.AST.Expressions.*;
import tinyjava.AnalizadorSemantico.AST.Sentences.*;
import tinyjava.Ejecutador;

public class SyntacticAnalyzer {
    private ParserUtils parserUtils;
    private SymbolTable table;
    private String[] OpAll = { "opAnd", "opOr", "opEquiv", "opDif", "opMenor", "opMayor", "opMenorIgual",
            "opMayorIgual", "opSum", "opRest", "opMult", "opDiv", "parCierra", "comma", "semicolon" };



    public SyntacticAnalyzer(LexicalAnalyzer lexicalAnalyzer) {
        parserUtils = new ParserUtils(lexicalAnalyzer);
    }

    /**
     * Produccion inicial.
     */
    public SymbolTable start() {
        table = new SymbolTable();
        claseMain();
        startF();
        // MainSemantic mSintactico = new MainSemantic(table);
        SemanticAnalyzer mAnalyzer = new SemanticAnalyzer(table);
        mAnalyzer.InitAnalysis();
        return table;

    }

    /**
     * Produccion obtenida de factorizar Start.
     */
    public void startF() {
        if (parserUtils.EOF()) {
            return;
        }
        otrasClases();
    }

    public void clase() {
        parserUtils.match("class");
        parserUtils.match("id");
        Token token = parserUtils.actualToken;
        if (table.classExists(token.getLexicon())) {
            HandleExceptionsSemantic(new ExistingClass(token.getLineNum(), token.getColNum(), token.getLexicon()));
        }
        ClassEntry nclass = new ClassEntry(token.getLexicon(), token.getLineNum(), token.getColNum(), table);
        table.setCurrentClass(nclass);
        claseF();
    }

    public void claseF() {
        if (parserUtils.CompareToNextToken("extends")) {
            claseConHerencia();
        } else {
            claseSinHerencia();
        }
    }

    public void claseSinHerencia() {
        table.getCurrentClass().inheritsFrom("Object");
        parserUtils.match("llaveAbre");
        // aca deberia llamar a claseSinHerenciaF pero el codigo es el mismo
        claseConHerenciaF();
    }

    public void claseConHerencia() {
        String her = herencia();
        if (her.equals("System")) {
            this.HandleExceptionsSemantic(new SemanticException("No se puede heredar la clase System.",
                    parserUtils.actualToken.getLineNum(), parserUtils.actualToken.getColNum()));
        }
        table.getCurrentClass().inheritsFrom(her);
        parserUtils.match("llaveAbre");
        claseConHerenciaF();
    }

    public void claseConHerenciaF() {
        if (parserUtils.CompareToNextToken("llaveCierra")) {
            parserUtils.match("llaveCierra");
            table.addCurrentClass();
            return;
        }
        miembros();
        table.addCurrentClass();
        parserUtils.match("llaveCierra");
    }

    public void otrasClases() {
        clase();
        otrasClasesF();
    }

    public void otrasClasesF() {
        if (parserUtils.EOF()) // siguientes start
        {
            return;
        }
        otrasClases();
    }

    public void claseMain() {
        parserUtils.match("class");
        parserUtils.match("id");
        Token token = parserUtils.actualToken;
        ClassEntry nclass = new ClassEntry(token.getLexicon(), token.getLineNum(), token.getColNum(), table);
        table.setCurrentClass(nclass);

        if (parserUtils.CompareToNextToken("extends")) {
            mainConHerencia();
            return;
        }
        mainSinHerencia();
    }

    public void mainConHerencia() {
        String her = herencia();
        if (her.equals("System")) {
            this.HandleExceptionsSemantic(new SemanticException("No se puede heredar la clase System.",
                    parserUtils.actualToken.getLineNum(), parserUtils.actualToken.getColNum()));
        }

        table.getCurrentClass().inheritsFrom(her);
        parserUtils.match("llaveAbre");
        parserUtils.match("static");
        parserUtils.match("void");
        parserUtils.match("id");
        if (!parserUtils.CompareByLexeme("main")) {
            parserUtils.HandleExceptions("La clase main requiere un método llamado \"main\"");
        }
        // añado metodo main a la clase
        MethodEntry mEntry = new MethodEntry("main", parserUtils.actualToken.getLineNum(),
                parserUtils.actualToken.getLineNum(), table, "static", new TypeVoid());
        table.getCurrentClass().addMethod(mEntry);
        table.setCurrentMethod(mEntry);

        parserUtils.match("parAbre");
        parserUtils.match("parCierra");
        BlockNode block = bloque();
        mEntry.setBlock(block);

        mainConHerenciaF();
    }

    public void mainConHerenciaF() {
        if (parserUtils.CompareToNextToken("llaveCierra")) {
            parserUtils.match("llaveCierra");
        } else {
            miembros();
            parserUtils.match("llaveCierra");
        }
        table.addCurrentClass();
    }

    public String herencia() {
        parserUtils.match("extends");
        parserUtils.match("id");
        return parserUtils.actualToken.getLexicon();
    }

    public void mainSinHerencia() {
        table.getCurrentClass().inheritsFrom("Object");
        parserUtils.match("llaveAbre");
        parserUtils.match("static");
        parserUtils.match("void");
        parserUtils.match("id");
        if (!parserUtils.CompareByLexeme("main")) {
            parserUtils.HandleExceptions("La clase main requiere un método llamado \"main\"");
        }
        // añado metodo main a la clase
        MethodEntry mEntry = new MethodEntry("main", parserUtils.actualToken.getLineNum(),
                parserUtils.actualToken.getLineNum(), table, "static", new TypeVoid());
        table.getCurrentClass().addMethod(mEntry);
        table.setCurrentMethod(mEntry);

        parserUtils.match("parAbre");
        parserUtils.match("parCierra");
        BlockNode block = bloque();
        mEntry.setBlock(block);

        mainSinHerenciaF();
    }

    public void mainSinHerenciaF() {
        if (parserUtils.CompareToNextToken("llaveCierra")) {
            parserUtils.match("llaveCierra");
            table.addCurrentClass();
            return;
        }
        miembros();
        table.addCurrentClass();
        parserUtils.match("llaveCierra");
    }

    public BlockNode miembros() {
        BlockNode block = miembro();
        miembrosF();
        return block;
    }

    public void miembrosF() {
        if (parserUtils.CompareToNextToken("llaveCierra")) // siguientes miembros
        {
            return;
        }
        miembros();
    }

    public BlockNode miembro() {
        // me fijo si es atributo
        if (parserUtils.CompareToNextToken("public") || parserUtils.CompareToNextToken("private")) {
            atributo();
            return null;
        } else if (parserUtils.CompareToNextToken("id")) // me fijo si es constructor
        {
            return constructor();
        } else {
            return metodo();
        }
    }

    public BlockNode metodo() {
        String form = forma_metodo();
        Type t = tipo_metodo();
        parserUtils.match("id");
        Token tkn = parserUtils.actualToken;
        if (table.getCurrentClass().methodExists(tkn.getLexicon())) {
            HandleExceptionsSemantic(new ExistingMetod(tkn.getLineNum(), tkn.getColNum(), tkn.getLexicon()));
            return null;
        }
        MethodEntry m = new MethodEntry(tkn.getLexicon(), tkn.getLineNum(), tkn.getColNum(), table, form, t);
        table.setCurrentMethod(m);
        table.getCurrentClass().addMethod(m);
        argumentos_formales();
        return bloque();
    }

    public String forma_metodo() {
        if (parserUtils.CompareToNextToken("static")) {
            parserUtils.match("static");
            return "static";
        } else if (parserUtils.CompareToNextToken("nostatic")) {
            parserUtils.match("nostatic");
            return "nostatic";
        } else {
            parserUtils.HandleExceptions("Mal formado el metodo.");
            return null;
        }
    }

    public Type tipo_metodo() {
        if (parserUtils.CompareToNextToken("void")) {
            parserUtils.match("void");
            return new TypeVoid();
        }
        return tipo();
    }

    public BlockNode constructor() {
        parserUtils.match("id");
        Token tkn = parserUtils.actualToken;
        if (table.getCurrentClass().hasConstructor()) {
            HandleExceptionsSemantic(new CtorExistsException(tkn.getLineNum(), tkn.getColNum(), tkn.getLexicon()));
        }

        CtorEntry m = new CtorEntry(tkn.getLexicon(), tkn.getLineNum(), tkn.getColNum(), table);
        table.getCurrentClass().addConstructor(m);
        table.setCurrentMethod(m);
        argumentos_formales();
        return bloque();
    }

    public void argumentos_formales() {
        parserUtils.match("parAbre");
        argumentos_formalesF();
    }

    public void argumentos_formalesF() {
        if (parserUtils.CompareToNextToken("parCierra")) {
            parserUtils.match("parCierra");
            return;
        } else {
            lista_argumentos_formales();
            parserUtils.match("parCierra");
        }
    }

    public void lista_argumentos_formales() {
        argumento_formal();
        lista_argumentos_formalesF();
    }

    public void lista_argumentos_formalesF() {
        if (parserUtils.CompareToNextToken("parCierra")) // siguiente lista_args_formales
        {
            return;
        }
        parserUtils.match("comma");
        lista_argumentos_formales();
    }

    public void argumento_formal() {
        Type t = tipo();
        parserUtils.match("id");
        Token tkn = parserUtils.actualToken;
        if (!table.getCurrentMethod().ParameterExists(tkn.getLexicon())) {
            VariableEntry vEntry = new VariableEntry(tkn.getLexicon(), tkn.getLineNum(), tkn.getColNum(), table, t);
            table.getCurrentMethod().addParameter(vEntry);
        } else {
            HandleExceptionsSemantic(new RepeatedParameter(tkn.getLineNum(), tkn.getColNum(), tkn.getLexicon(),
                    table.getCurrentMethod().getName()));
            return;
        }
    }

    public void atributo() {
        String vis = visibilidad();
        Type t = tipo();
        lista_declaracion_variables(t, vis);
        parserUtils.match("semicolon");
    }

    public String visibilidad() {
        if (parserUtils.CompareToNextToken("public")) {
            parserUtils.match("public");
            return "public";
        } else if (parserUtils.CompareToNextToken("private")) {
            parserUtils.match("private");
            return "private";
        } else {
            parserUtils.HandleExceptions("Visibilidad incorrecta. Se esperaba public o private.");
            return null;
        }
    }

    public BlockNode bloque() {
        parserUtils.match("llaveAbre");
        // bloque sin sentencias
        BlockNode block = new BlockNode(parserUtils.actualToken,table.getCurrentMethod().getCurrentScope());
        if (parserUtils.CompareToNextToken("llaveCierra")) {
            parserUtils.match("llaveCierra");
            return block;
        }
        LinkedList<SentenceNode> sentences = new LinkedList<SentenceNode>();
        sentencias(sentences);
        for (SentenceNode sentenceNode : sentences) {
            block.addSentence(sentenceNode);
        }
        parserUtils.match("llaveCierra");
        // print bloque
        // block.checkNode();
        table.getCurrentMethod().setBlock(block);
        return block;
    }

    public void sentencias(LinkedList<SentenceNode> sentences) {
        SentenceNode sent = sentencia();
        sentences.add(sent);
        sentenciasF(sentences);
    }

    public void sentenciasF(LinkedList<SentenceNode> sentences) {
        if (parserUtils.CompareToNextToken("llaveCierra")) // siguiente sentencias
        {
            return;
        }
        sentencias(sentences);
    }

    public SentenceNode sentencia() {
        String nextToken = parserUtils.nextToken.getToken();
        SentenceNode sent;
        sent = new SentenceNode(parserUtils.actualToken,table.getCurrentMethod().getCurrentScope());
        if (nextToken.equals("semicolon")) {
            parserUtils.match("semicolon");
        } else if (nextToken.equals("this")) {
            parserUtils.match("this");
            sent = asignacion();
        } else if (nextToken.equals("parAbre")) {
            sent = llamada();
        } else if (nextToken.equals("tString") || nextToken.equals("tBool") || nextToken.equals("tChar")
                || nextToken.equals("tInt")) {
            declaracion(false);
        } else if (nextToken.equals("while")) {
            sent = whileSent();
        } else if (nextToken.equals("if")) {
            sent = ifSent();
        } else if (nextToken.equals("llaveAbre")) {
            sent = bloque();
        } else if (nextToken.equals("id")) {
            parserUtils.match("id");
            if (parserUtils.CompareToNextToken("id")) // si vuelve a ser un id es una declaracion
            {
                declaracion(true);
            } else {
                sent = asignacion();
            }
        } else {
            sent = retSent();
        }

        return sent;
    }

    public AssignationNode asignacion() {
        Token tkn = parserUtils.actualToken;

        Encadenado enc = EncadenadoF();
        
        parserUtils.match("opIgual");
        ExpressionNode exp = expresion();
        AssignationNode assignationNode = new AssignationNode(tkn, exp,enc,table.getCurrentMethod().getCurrentScope());
        parserUtils.match("semicolon");
        return assignationNode;
    }

    public ExpressionNode expresion() {

        ExpressionNode exp = Expresion();
        return exp;
    }

    public void declaracion(boolean isId) {
        Type tipo;
        if (!isId)
            tipo = tipo();
        else
            tipo = new TypeRef(parserUtils.actualToken.getLexicon(), "ref");
        lista_declaracion_variables(tipo, null);
    }

    public void lista_declaracion_variables(Type tipo, String vis) {
        parserUtils.match("id");
        Token tokn = parserUtils.actualToken;
        //si la visibilidad es null, es una variable local
        if (vis == null) {
            if (table.getCurrentMethod().VariableExists(tokn.getLexicon())) {
                HandleExceptionsSemantic(new RepeatedParameter(
                    tokn.getLineNum(), 
                    tokn.getColNum(), 
                    tokn.getLexicon(),
                    table.getCurrentMethod().getName()
                ));
            }

            VariableEntry vEntry = new VariableEntry(tokn.getLexicon(), tokn.getLineNum(), tokn.getColNum(), table,
                    tipo);
            table.getCurrentMethod().addLocalVar(vEntry);
        } 
        //caso contrario es un atributo
        else {
            if (table.getCurrentClass().attributeExists(tokn.getLexicon())) {
                HandleExceptionsSemantic(new RepeatedAttrException(
                    tokn.getLineNum(), 
                    tokn.getColNum(),
                    tokn.getLexicon(), 
                    table.getCurrentClass().getName()
                ));
            }

            AttributeEntry vEntry = new AttributeEntry(tokn.getLexicon(), tokn.getLineNum(), tokn.getColNum(), table,
                    tipo, vis);
            table.getCurrentClass().addAttribute(vEntry);
        }
        lista_declaracion_variablesF(tipo, vis);
    }

    public void lista_declaracion_variablesF(Type tipo, String vis) {
        if (parserUtils.nextToken.getToken().equals("comma")) {
            parserUtils.match("comma");
            lista_declaracion_variables(tipo, vis);
        } else if (parserUtils.CompareToNextToken("semicolon")) {
            // parserUtils.match("semicolon");
            return;
        } else {
            // error
            parserUtils.HandleExceptions("Fallo al declarar la variable " + parserUtils.actualToken.getLexicon());
        }

    }

    public Type tipo() {
        String[] tipos = { "tString", "tBool", "tChar", "tInt", "id" };
        String nexToken = parserUtils.NextTokenToString();
        for (int i = 0; i < tipos.length; i++) {
            if (nexToken.equals(tipos[i])) {
                parserUtils.match(tipos[i]);
                Type nt;
                nt = new TypeRef(parserUtils.actualToken.getLexicon(), "ref");
                switch (tipos[i]) {
                    case "tString":
                        nt = new TypeString(parserUtils.actualToken.getLexicon());
                        break;
                    case "tBool":
                        nt = new TypeBoolean(parserUtils.actualToken.getLexicon());
                        break;
                    case "tChar":
                        nt = new TypeChar(parserUtils.actualToken.getLexicon());
                        break;
                    case "tInt":
                        nt = new TypeInt(parserUtils.actualToken.getLexicon());
                }
                return nt;
            }
        }
        parserUtils.HandleExceptions("Tipo de dato ingresado no existe.");
        return null;
    }

    public WhileNode whileSent() {
        parserUtils.match("while");
        Token token = parserUtils.actualToken;
        parserUtils.match("parAbre");
        ExpressionNode condition = expresion();
        parserUtils.match("parCierra");

        //guardo branch actual y creo nuevo branch
        int currentScope = table.getCurrentMethod().getCurrentScope();
        table.getCurrentMethod().branchScope();
        SentenceNode sent = sentencia();
        
        //retorno al branch anterior al if
        table.getCurrentMethod().returnToScope(currentScope);

        return new WhileNode(token, condition, sent,table.getCurrentMethod().getCurrentScope());
    }

    public CallNode llamada() {
        parserUtils.match("parAbre");
        Token tkn = parserUtils.actualToken;
        PrimaryNode called = Primario();
        parserUtils.match("parCierra");
        return new CallNode(tkn, called,table.getCurrentMethod().getCurrentScope());
    }

    public IfNode ifSent() {
        parserUtils.match("if");
        Token token = parserUtils.actualToken;
        parserUtils.match("parAbre");
        ExpressionNode condition = expresion();
        parserUtils.match("parCierra");
        
        //guardo branch actual y creo nuevo branch
        int currentScope = table.getCurrentMethod().getCurrentScope();
        table.getCurrentMethod().branchScope();
        SentenceNode sent = sentencia();
        
        //retorno al branch anterior al if
        table.getCurrentMethod().returnToScope(currentScope);
        Optional<SentenceNode> elseSent = ifSentF();
        if (elseSent.isPresent()) {
            //retorno al branch anterior al else
            table.getCurrentMethod().returnToScope(currentScope);
            return new IfElseNode(token, condition, sent, elseSent.get(),table.getCurrentMethod().getCurrentScope());
        }
        return new IfNode(token, condition, sent,table.getCurrentMethod().getCurrentScope());
    }

    public Optional<SentenceNode> ifSentF() {
        String nexToken = parserUtils.NextTokenToString();
        if (nexToken.equals("else")) {
            parserUtils.match("else");
            //creo nuevo scope
            table.getCurrentMethod().branchScope();
            return Optional.of(sentencia());
        } else 
        {
            return Optional.empty();
        }
    }

    public ReturnNode retSent() {
        parserUtils.match("return");
        Token token = parserUtils.actualToken;
        ExpressionNode exp = retSentF();

        if (exp != null)
            return new ReturnNode(token, exp.getType(), exp,table.getCurrentMethod().getCurrentScope());
        else
            return new EmptyRtnNode(parserUtils.actualToken,table.getCurrentMethod().getCurrentScope());
    }

    public ExpressionNode retSentF() {
        if (parserUtils.CompareToNextToken("semicolon")) {
            parserUtils.match("semicolon");
            return null;
        }
        ExpressionNode exp = expresion();
        parserUtils.match("semicolon");
        return exp;
    }

    public ExpressionNode Expresion() {

        ExpressionNode exp = ExpOr();

        return exp;
    }

    public ExpressionNode ExpOr() {
        ExpressionNode exp1 = ExpAnd();
        ExpressionNode exp = ExpOrR(exp1);
        //exp.checkNode();
        return exp;
    }

    public ExpressionNode ExpOrR(ExpressionNode expR) {
        String[] OpCom = { "opOr" };
        if (parserUtils.matchList(OpCom)) {
            Token tokenActT = parserUtils.actualToken;
            ExpressionNode exp1 = ExpAnd();
            ExpressionNode exp2 = ExpOrR(exp1);
            return new BinaryExpNode(new TypeVoid(), tokenActT, expR, exp2,table.getCurrentMethod().getCurrentScope());
        } else {
            if (parserUtils.matchListStill(OpAll)) {
                return expR;
                // lamda
            } else {
                parserUtils.HandleExceptions("Falta completar expresión o ;");
                return null;
            }
        }
    }

    public ExpressionNode ExpAnd() {
        ExpressionNode exp1 = ExpIgual();
        ExpressionNode exp = ExpAndR(exp1);
        return exp;
    }

    public ExpressionNode ExpAndR(ExpressionNode expR) {
        String[] OpCom = { "opAnd" };
        if (parserUtils.matchList(OpCom)) {
            Token tokenActT = parserUtils.actualToken;
            ExpressionNode exp1 = ExpIgual();
            ExpressionNode exp2 = ExpAndR(exp1);
            return new BinaryExpNode(new TypeVoid(), tokenActT, expR, exp2,table.getCurrentMethod().getCurrentScope());
        } else {
            if (parserUtils.matchListStill(OpAll)) {
                return expR;
                // lamda
            } else {
                parserUtils.HandleExceptions("Falta completar expresión o ;");
                return null;
            }
        }
    }

    public ExpressionNode ExpIgual() {
        ExpressionNode exp1 = ExpCompuesta();
        ExpressionNode exp = ExpIgualR(exp1);
        return exp;
    }

    public ExpressionNode ExpIgualR(ExpressionNode expR) {
        String[] OpIgual = { "opEquiv", "opDif" };
        if (parserUtils.matchList(OpIgual)) {
            Token tokenActT = parserUtils.actualToken;
            ExpressionNode exp1 = ExpCompuesta();
            ExpressionNode exp2 = ExpIgualR(exp1);
            return new BinaryExpNode(new TypeVoid(), tokenActT, expR, exp2,table.getCurrentMethod().getCurrentScope());
        } else {
            if (parserUtils.matchListStill(OpAll)) {
                return expR;
                // lamda
            } else {
                parserUtils.HandleExceptions("Falta completar expresión o ;");
                return null;
            }
        }
    }

    public ExpressionNode ExpCompuesta() {
        ExpressionNode exp1 = ExpAd();
        ExpressionNode exp = ExpCompuestaF(exp1);
        return exp;
    }

    public ExpressionNode ExpCompuestaF(ExpressionNode expR) {
        String[] OpCompuesto = { "opMenor", "opMayor", "opMenorIgual", "opMayorIgual" };
        if (parserUtils.matchList(OpCompuesto)) {
            Token tokenActT = parserUtils.actualToken;
            ExpressionNode exp1 = ExpAd();
            ExpressionNode exp2 = ExpCompuestaF(exp1);
            return new BinaryExpNode(new TypeVoid(), tokenActT, expR, exp2,table.getCurrentMethod().getCurrentScope());
        } else {
            if (parserUtils.matchListStill(OpAll)) {
                return expR;
                // lamda
            } else {
                parserUtils.HandleExceptions("Falta completar expresión o ;");
                return null;
            }
        }
    }

    public ExpressionNode ExpAd() {
        ExpressionNode exp1 = ExpMul();
        ExpressionNode exp = ExpAdR(exp1);
        return exp;
    }

    public ExpressionNode ExpAdR(ExpressionNode expR) {
        String[] OpAd = { "opSum", "opRest" };
        if (parserUtils.matchList(OpAd)) {
            Token tokenActT = parserUtils.actualToken;
            ExpressionNode exp1 = ExpMul();
            ExpressionNode exp2 = ExpAdR(exp1);
            return new BinaryExpNode(new TypeVoid(), tokenActT, expR, exp2,table.getCurrentMethod().getCurrentScope());
        } else {
            if (parserUtils.matchListStill(OpAll)) {
                return expR;
                // lamda
            } else {
                parserUtils.HandleExceptions("Falta completar expresión o ;");
                return null;
            }
        }
    }

    public ExpressionNode ExpMul() {
        ExpressionNode exp1 = ExpUn();
        ExpressionNode exp = ExpMulR(exp1);
        return exp;
    }

    public ExpressionNode ExpMulR(ExpressionNode expR) {
        String[] OpMul = { "opMult", "opDiv" };
        if (parserUtils.matchList(OpMul)) {
            Token tokenActT = parserUtils.actualToken;
            ExpressionNode exp1 = ExpUn();
            ExpressionNode exp2 = ExpMulR(exp1);
            return new BinaryExpNode(new TypeVoid(), tokenActT, expR, exp2,table.getCurrentMethod().getCurrentScope());
        } else {
            if (parserUtils.matchListStill(OpAll)) {
                return expR;
                // lamda
            } else {
                parserUtils.HandleExceptions("Falta completar expresión o ;");
                return null;
            }
        }
    }

    public ExpressionNode ExpUn() {
        String[] OpUnario = { "opPos", "opNeg", "opNot" };
        String[] OpAd = { "opSum", "opRest" , "opNot"};
        if (parserUtils.matchList(OpAd)) {
            Token tokenActT = parserUtils.actualToken;
            ExpressionNode exp = ExpUn();
            return new UnaryExpNode(exp.getType(), tokenActT, exp,table.getCurrentMethod().getCurrentScope());
        } else {
            return Operando();
        }
    }

    public ExpressionNode Operando() {
        String[] Literal = { "null", "true", "false", "number", "char", "string" };
        Token nToken = parserUtils.nextToken;
        String nextToken = parserUtils.nextToken.getToken();
        if (parserUtils.matchList(Literal)) {
            // literal numero. Ej: 5
            if (nextToken.equals("number")) {
                return new LiteralNode(new TypeInt(nToken.getLexicon()), parserUtils.actualToken,table.getCurrentMethod().getCurrentScope());
            }
            // literal caracter. Ej: "c"
            if (nextToken.equals("char")) {
                return new LiteralNode(new TypeChar(nToken.getLexicon()), parserUtils.actualToken,table.getCurrentMethod().getCurrentScope());
            }
            // literal String. Ej: "hola"
            if (nextToken.equals("string")) {
                return new LiteralNode(new TypeString(nToken.getLexicon()), parserUtils.actualToken,table.getCurrentMethod().getCurrentScope());
            }
            // literal false
            if (nextToken.equals("false")) {
                return new LiteralNode(new TypeBoolean(nToken.getLexicon()), parserUtils.actualToken,table.getCurrentMethod().getCurrentScope());
            }
            // literal true
            if (nextToken.equals("true")) {
                return new LiteralNode(new TypeBoolean(nToken.getLexicon()), parserUtils.actualToken,table.getCurrentMethod().getCurrentScope());
            }

            return new LiteralNode(new TypeVoid(), parserUtils.actualToken,table.getCurrentMethod().getCurrentScope());

        } else {
            ExpressionNode exp = Primario();
            return exp;
        }
    }

    public PrimaryNode Primario() {
        PrimaryNode primaryNode;
        String nextToken = parserUtils.nextToken.getToken();
        // expresión parentizada. Ej: (true)
        if (nextToken.equals("parAbre")) {
            parserUtils.match("parAbre");
            Token tokenActT = parserUtils.actualToken;
            ExpressionNode exp = expresion();
            nextToken = parserUtils.nextToken.getToken();
            parserUtils.match("parCierra");
            Encadenado enc = EncadenadoF();
            primaryNode = new ParenthesisNode(new TypeVoid(), tokenActT, enc, exp,table.getCurrentMethod().getCurrentScope());
            return primaryNode;
        }
        // acceso this. Ej: this.m1();
        else if (nextToken.equals("this")) {
            Token tokenActT = parserUtils.nextToken;
            parserUtils.match("this");
            Encadenado enc = EncadenadoF();
            return new ThisNode(new TypeVoid(), tokenActT, enc,table.getCurrentMethod().getCurrentScope());
        }
        // acceso var/metodo/metodo estatico. Ej: id.a; id.m1(); m1();
        else if (nextToken.equals("id")) {
            Token tokenActT = parserUtils.nextToken;
            parserUtils.match("id");
            // return Var_metodoF2();
            if(parserUtils.CompareToNextToken("parAbre") )
                return new MethodNode(new TypeVoid(), tokenActT, Var_metodoF(),table.getCurrentMethod().getCurrentScope());
            else
                return new VarNode(new TypeVoid(), tokenActT, Var_metodoF(),table.getCurrentMethod().getCurrentScope());
        }
        // llamada a constructor. Ej: new ClaseA(a);
        else if (nextToken.equals("new")) {
            parserUtils.match("new");
            Token tokenActTem = parserUtils.actualToken;
            parserUtils.match("id");
            Token tokenActT = parserUtils.actualToken;
            ArrayList<ExpressionNode> argumentos = Argumentos_Actuales();
            Encadenado enc = EncadenadoF();
            Encadenado encList = new LLamadaEncadenado(new TypeVoid(), tokenActT, enc, argumentos,table.getCurrentClass().getName(),table.getCurrentMethod().getName(),table.getCurrentMethod().getCurrentScope());
            primaryNode = new NewNode(new TypeVoid(), tokenActT, encList,table.getCurrentMethod().getCurrentScope());
            return primaryNode;
        } else {
            // Error
            parserUtils.HandleExceptions("Expresión invalida.");
            return null;
        }

    }

    public Encadenado EncadenadoF() {
        String nextToken = parserUtils.nextToken.getToken();
        if (nextToken.equals("dot")) {
            parserUtils.match("dot");
            nextToken = parserUtils.nextToken.getToken();
            if (!parserUtils.CompareToNextToken("id")) {
                parserUtils.HandleExceptions("Falta id");
            }
            parserUtils.match("id");
            return EncadenadoFactorizado();
        } else {
            String[] OpAll_ParAbre_Comma = { "opAnd", "opOr", "opEquiv", "opDif", "opMenor", "opMayor", "opMenorIgual",
                    "opMayorIgual", "opSum", "opRest", "opMult", "opDiv", "parCierra", "comma", "opIgual",
                    "semicolon" };

            if (!parserUtils.matchListStill(OpAll_ParAbre_Comma)) {
                parserUtils.HandleExceptions("Se espera expresion de la forma id.id ");
                return null;
            } else {
                // lamda
                return null;
                
            }
        }
    }


    public Encadenado Var_metodoF() {
        String nextToken = parserUtils.nextToken.getToken();
        // si el siguiente token es un "." es una llamada a un metodo estatico.
        if (nextToken.equals("dot")) {
            return EncadenadoF();  
        }
        // si el siguiente token es un parentesis es una llamada a un metodo
        else if (nextToken.equals("parAbre")) {
            return Llamada_Metodo();
        } else {
            String[] OpAll_ParAbre_Comma = { "opAnd", "opOr", "opEquiv", "opDif", "opMenor", "opMayor", "opMenorIgual",
                    "opMayorIgual", "opSum", "opRest", "opMult", "opDiv", "parCierra", "comma", "opIgual",
                    "semicolon" };
            if (!parserUtils.matchListStill(OpAll_ParAbre_Comma)) {
                // Error
                parserUtils.HandleExceptions("Expresión inválida.");
                return null;
            }else{
                //lambda
                return null; //sin encadenado
            }
        }
    }

    public Encadenado Llamada_Metodo() {
        Token tokenActTem = parserUtils.actualToken;
        ArrayList<ExpressionNode> argumentos = Argumentos_Actuales();
        Encadenado enc = EncadenadoF();
        return new LLamadaEncadenado(new TypeVoid(), tokenActTem, enc, argumentos,table.getCurrentClass().getName(),table.getCurrentMethod().getName(),table.getCurrentMethod().getCurrentScope());
    }

    public ArrayList<ExpressionNode> Argumentos_Actuales() {
        parserUtils.match("parAbre");
        return Argumentos_ActualesF();
    }

    public ArrayList<ExpressionNode> Argumentos_ActualesF() {
        ArrayList<ExpressionNode> expLis = new ArrayList<ExpressionNode>();
        String nextToken = parserUtils.nextToken.getToken();
        if (nextToken.equals("parCierra")) {
            parserUtils.match("parCierra");
            return expLis;
        } else {
            expLis = Lista_Expresiones(expLis);
            if (!parserUtils.CompareToNextToken("parCierra")) {
                parserUtils.HandleExceptions("Se esperaba el símbolo \")\".");
            }
            parserUtils.match("parCierra");
            return expLis;
        }
    }

    public ArrayList<ExpressionNode> Lista_Expresiones(ArrayList<ExpressionNode> expLis) {
        ExpressionNode exp = expresion();
        expLis.add(exp);
        return Lista_ExpresionesF(expLis);

    }

    public ArrayList<ExpressionNode> Lista_ExpresionesF(ArrayList<ExpressionNode> expLis) {
        String nextToken = parserUtils.nextToken.getToken();
        if (nextToken.equals("comma")) {
            parserUtils.match("comma");
            return Lista_Expresiones(expLis);
        } else if (nextToken.equals("parCierra")) { // Siguientes de Lista-Expresiones
            // Lamda
            return expLis;
        } else {
            // error
            parserUtils.HandleExceptions("Lista sin cerrar, falta ) o ,");
            return null;
        }
    }

    public Encadenado EncadenadoFactorizado() {
        String nextToken = parserUtils.nextToken.getToken();
        Token tokenActTem = parserUtils.actualToken;
        if (nextToken.equals("parAbre")) {
            ArrayList<ExpressionNode> argumentos = Argumentos_Actuales();
            Encadenado enc = EncadenadoF();

            return new LLamadaEncadenado(new TypeVoid(), tokenActTem, enc, argumentos,table.getCurrentClass().getName(),table.getCurrentMethod().getName(),table.getCurrentMethod().getCurrentScope());

        } else {
            Encadenado enc = EncadenadoF();
            return new VarEncadenado(new TypeVoid(), tokenActTem, enc,table.getCurrentMethod().getCurrentScope());
        }
    }

    public void HandleExceptionsSemantic(SemanticException e) {
        throw e;
    }
}