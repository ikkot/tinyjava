package tinyjava.AnalizadorSintactico;

import java.nio.Buffer;

import javax.crypto.Mac;

//import sun.font.TrueTypeFont;
import tinyjava.Token;
import tinyjava.AnalizadorLexico.*;

/**
 * @brief Clase con funciones en comun para el analizador sintáctico. estan en
 *        una clase separada para que podamos trabajar en conjunto sin tener
 *        problemas de merge.
 */
public class ParserUtils {
    private LexicalAnalyzer lexicalAnalyzer;
    /** < Analizador léxico. Retorna tokens. */
    public Token actualToken;
    /** < Token con el que se trabaja actualente. */
    public Token nextToken;
    /** < siguiente token por venir. */
    private boolean returned;

    public ParserUtils(LexicalAnalyzer lexicalAnalyzer) {
        this.lexicalAnalyzer = lexicalAnalyzer;
        nextToken = this.lexicalAnalyzer.NextToken();
    }

    /** Actualiza el token actual y el siguiente por venir. */
    private void updateToken() {

        if (tinyjava.Buffer.getInstance().EOF) {
            throw new SyntaxisException("se llegó al final del archivo inesperadamente.", tinyjava.Buffer.getInstance().GetLineNumber() , tinyjava.Buffer.getInstance().GetColNumber());
            // return;
        } else {
            this.actualToken = this.nextToken; // token actual
            if (returned) {
                returned = !returned;
                return;
            }
            this.nextToken = lexicalAnalyzer.NextToken(); // token siguiente. lo guardo para saber que viene despues
            if (tinyjava.Buffer.getInstance().EOF) {
                this.nextToken = this.actualToken;
                return;
            }
        }

    }

    /**
     * Compara un token con un nombre ingresado por parámetro. En caso de matchear
     * consume el token.
     */
    public void match(String tokenName) {
        genericMatch(tokenName, true);
    }

    /**
     * Matchea un token sin consumirlo.
     */
    @Deprecated
    public void matchStill(String tokenName) {
        genericMatch(tokenName, false);
    }

    /**
     * proceso generico de matchear. Contempla si al matchear se consume un token o
     * no.
     */
    private void genericMatch(String tokenName, boolean moving) {
        boolean matched = matching(tokenName, moving);

        if (!matched) {
            HandleExceptions("Se esperaba un " + actualToken.getToken()+" y se recibió un "+tokenName);
            return;
        }
    }

    /**
     * retorna verdadero si el nombre del token concuerda con el ingresado por
     * input.
     * 
     * @param tokenName:   nombre del token a matchear
     * @param updateToken: define si se consume el token o no.
     * @return bool.
     */
    private boolean matching(String tokenName, boolean updateToken) {
        if (updateToken)
            updateToken();
        return actualToken.getToken().equals(tokenName);
    }

    /**
     * Intenta matchear el token actual con una lista de nombres.
     * 
     * @param tiposL lista de tokens que se tratará de matchear.
     * @return bool
     */
    public Boolean matchList(String[] tiposL) {
        String nextTokenT = nextToken.getToken();
        for (int i = 0; i < tiposL.length; i++) {
            if (nextTokenT.equals(tiposL[i])) {
                match(tiposL[i]);
                return true;
            }
        }
        return false;
    }

    /**
     * Intenta matchear el token actual con una lista de nombres sin consumir caracter.
     * 
     * @param tiposL lista de tokens que se tratará de matchear.
     * @return bool
     */
    public Boolean matchListStill(String[] tiposL) {
        String nextTokenT = nextToken.getToken();
        for (int i = 0; i < tiposL.length; i++) {
            if (nextTokenT.equals(tiposL[i])) {
                return true;
            }
        }
        return false;
    }

    public Boolean listCheck(String[] tiposL,String tokenVal) {

        for (int i = 0; i < tiposL.length; i++) {
            if (tokenVal.equals(tiposL[i])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Devuelve el nombre del token por venir.
     * 
     * @return String.
     */
    public String NextTokenToString() {
        return nextToken.getToken();
    }

    /**
     * Compara un nombre de entrada con el nombre del token por venir.
     */
    public boolean CompareToNextToken(String token) {
        return nextToken.getToken().equals(token);
    }

    /**
     * Compara el lexema del token actual con un string ingresado.
     */
    public boolean CompareByLexeme(String token) {
        return actualToken.getLexicon().equals(token);
    }

    /**
     * Retorna true si se llegó al final del archivo.
     */
    public boolean EOF() {
        return tinyjava.Buffer.getInstance().EOF;
    }

    /**
     * Lanza una excepción de tipo sintáctica.
     * 
     * @param msg Mensaje de error que se muestra.
     */
    public void HandleExceptions(String msg) {
        Integer nextTokenL = actualToken.getLineNum();
        Integer nextTokenC = actualToken.getColNum();
        SyntaxisException e = new SyntaxisException(msg, nextTokenL, nextTokenC);
        throw e;
    }
}