package tinyjava.AnalizadorSintactico;

public class SyntaxisException extends RuntimeException
{
    public SyntaxisException(String msg,Integer line, Integer col)
    {
        super("Error: "+msg+" (Ln "+Integer.toString(line)+":Col "+Integer.toString(col)+")");
    }    
}