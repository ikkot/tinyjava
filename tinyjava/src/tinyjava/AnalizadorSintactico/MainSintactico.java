package tinyjava.AnalizadorSintactico;

import java.util.ArrayList;

import tinyjava.*;
import tinyjava.AnalizadorLexico.LexicalAnalyzer;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

public class MainSintactico 
{
    public static void main(String[] args) 
    {
        String path = System.getProperty("user.dir")+"/Tests/semantico/test";
        LexicalAnalyzer lexicalAn = new LexicalAnalyzer(path);
        SyntacticAnalyzer synSor = new SyntacticAnalyzer(lexicalAn);
        SymbolTable table = synSor.start();



    }
    
}