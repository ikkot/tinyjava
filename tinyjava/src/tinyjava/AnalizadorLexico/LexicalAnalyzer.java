package tinyjava.AnalizadorLexico;

import java.util.HashMap;
import tinyjava.*;
import tinyjava.AnalizadorLexico.Exceptions.*;

/** @brief Clase encargada de manejar todo el proceso de analisis léxico
 * 
 */
public class LexicalAnalyzer 
{
	/** Buffer utilizado para leer los caracteres de un script. */
	Buffer buffer; 
	private int lineNum; /**< Numero de linea actual. */
	private int columNum; /**< Numero de columna actual. */

	/**@param reserved - HashMap con todas las palabras reservadas del sistema. */
	private final HashMap<String, String> reserved = new HashMap<String, String>() {{ 
		put("class","class");
		put("extends","extends");
		put("static","static");
		put("nostatic","nostatic");
		put("String","tString");
		put("boolean","tBool");
		put("char","tChar");
		put("int","tInt");
		put("public","public");
		put("private","private");
		put("void","void");
		put("null","null");
		put("while","while");
		put("return","return");
		put("this","this");
		put("new","new");
		put("true","true");
		put("false","false");
		put("if","if");
		put("else","else");
	}};
	/**@param reserved - HashMap con todos los operadores. */
	private final HashMap<String, String> operators = new HashMap<String, String>() {{
		put("=","opIgual");
		put(">","opMayor");
		put("<","opMenor");
		put(">=","opMayorIgual");
		put("<=","opMenorIgual");
		put("==","opEquiv");
		put("+","opSum");
		put("-","opRest");
		put("*","opMult");
		put("/","opDiv");
		put("^","opExp");
		put("&&", "opAnd");
		put("||", "opOr");
		put("!=","opDif");
		put("!","opNot");
	}};
	
	/**@param reserved - HashMap con todos los separadores de tokens. */
	private final HashMap<String, String> separators = new HashMap<String, String>() {{
		put("line_break","separador");
		put("space","separador");
		put(";","semicolon");
		put("(","parAbre");
		put(")","parCierra");
		put("{","llaveAbre");
		put("}","llaveCierra");
		put("'","singleQuot");
		put("doubleQuot","doubleQuot");
		put(".","dot");
		put(",","comma");
	}};
	
	/**
	 * Constructor.
	 * @param path - Directorio donde está ubicado el script que se está compilando.
	 */
	public LexicalAnalyzer(final String path)
	{
		this.buffer = Buffer.getInstance();
		//this.buffer = new Buffer(path);
		buffer.openFile(path);
	}

	/**
	 * Retorna un token nuevo cada vez que es llamado.
	 */
	public Token NextToken() 
	{
		Token token = new Token();
		final String c = buffer.nexChar();
		if(c.equals("EOF") || c.equals("-2"))
		{
			return null;
		}

		lineNum = buffer.GetLineNumber();
		columNum = buffer.GetColNumber();
		if (c.equals("/")) 
		{
			final String aux = detectComent();
			if (aux.equals("div")) {
				token = new Token("/", "opDiv",lineNum,columNum);
			}else{
				token = NextToken();
			}
		}

		else if (separators.containsKey(c)) 
		{
			token = detectSeparator(c);
		}

		else if (operators.containsKey(c)||c.equals("|")||c.equals("&")) 
		{
			token = GetOperatorToken(c);
		}

		else if (CheckIfNumber(c)) {
			// crea un token de tipo int y se asegura que no hayan letras
			token = GetNumberToken(c);
		}
		else if (c.equals("EOF")) { 
			buffer.closeFile();
			return null;
		}
		
		else
		{
			token =  GetIdentiferToken(c);
		}
		return token;
	}

	/**
	 * Devuelve true si c es un número y false en caso contrario.
	 */
	private boolean CheckIfNumber(final String c) {
		try {
			final int i = Integer.parseInt(c);
		} catch (final NumberFormatException e) {
			return false;
		}
		return true;
	}

	/**
	 * Devuelve div si se detecta que es una divicion valida, comment si es un comentario valido y null si es un comentario invalido
	 * @return String state
	 */
	private String detectComent() {

		String aux = buffer.seeWithoutConsume();
		String state = "";
		if (aux.equals("*")) // comentario
		{
			aux = buffer.nexChar();
			while (true) 
			{
				aux = buffer.nexChar();
				if (aux.equals("*")) 
				{
					aux = buffer.nexChar();
					if (aux.equals("/")) 
					{
						state = "comment";
						break;// cometario valido
					}else if(aux.equals("EOF")) {
						HandleExceptions(new EndOfFileException(lineNum, columNum));
						return null;
					}
				} 
				else if (aux.equals("EOF"))
				{
					HandleExceptions(new EndOfFileException(lineNum, columNum));
					return null;
				}
				else if (aux.equals("/"))
				{
					state =  detectComent();
				}
			}
		} else { // detectar division
			state = "div";
		} 
		return state;
	}
	
	/**
	 * Detecta si un caracter es un separador y lo retorna como Token.
	 * @param car Caracter actual.
	 * @return Token separador.
	 */
	private Token detectSeparator(final String car)
	{
		final String tok=separators.get(car);

		if(tok.equals("separador"))
		{
			return NextToken();
		}
		else if(tok.equals("doubleQuot"))
		{
			return GetStringToken();
		}
		else if(tok.equals("singleQuot"))
		{
			return GetCharacterToken();
		}
		return new Token(car, tok,lineNum,columNum);
	}

	/**
	 * Retorna un caracter en forma de Token.
	 */
	private Token GetCharacterToken()
	{
		String lex = "";
		String nc ="";
		int i = 0;
		while(true)
		{
			nc = buffer.nexChar();
			i++;
			//encuentro barra invertida, si o si tiene que haber otro caracter
			if(nc.equals("\\"))
			{
				// lex += nc;
				nc = buffer.nexChar();
				if(nc.equals("'") && nc.length()>1)
				{
					HandleExceptions(new InvalidCharacterException(lineNum, columNum));
					return null;
				}
				lex = nc.equals("n") ? "line_break" : nc;
				lex = nc.equals("t") ? "tab" : nc;
				i++;
				continue;
			}

			if(nc.equals("'"))
			{
				break;
			}

			if(nc.equals("EOF"))
			{
				HandleExceptions(new UnclosedLiteralException(lineNum, columNum, false));
			}

			//los caracteres no pueden ser mas largos que 2.
			if(i>=2)
			{	
				HandleExceptions(new InvalidCharacterException(lineNum, columNum));
				return null;				
			}

			lex += nc;
		}

		return new Token(lex,"char",lineNum,columNum);
	}

	/**
	 * Retorna un string en forma de Token
	 * @return
	 */
	private Token GetStringToken()
	{
		String lex = "";
		String nc = "";
		int row = buffer.GetLineNumber();
		int col = buffer.GetColNumber();
		while(true)
		{
			nc = buffer.nexChar();
			if(nc.equals("doubleQuot"))
			{
				break;
			}
			if(nc.equals("EOF"))
			{
				HandleExceptions(new UnclosedLiteralException(lineNum, columNum,true));
				return null;
			}
			if(nc.equals("space"))
			{
				nc = " ";
			}
			lex += nc;
		}

		return new Token(lex,"string",row,col);
	}

	/**
	 * Retorna un número en forma de Token.
	 * @param c primer caracter del número.
	 */
	private Token GetNumberToken(final String c){
		String nro = c;
		String nc;
		Boolean cero = true;
		while(true)
		{
			nc = this.buffer.seeWithoutConsume();
			//si me encuentro un separador o un operador corto
			if(separators.get(nc) != null || operators.get(nc) != null || buffer.EOF)
			{
				break;
			}
			//si el nuevo caracter no es un numero, error lexico
			if(!CheckIfNumber(nc))
			{
				HandleExceptions(new InvalidNumberException(lineNum, columNum));
				return null;
			}else
			{
				if (cero == false){
					nro += buffer.nexChar();
				}else if (nc.equals("0")){
					buffer.nexChar();
					nro="";
				}else{
					nro += buffer.nexChar();
					System.out.println("");
					cero = false;
				}
			}
		}
		return new Token(nro,"number",lineNum,columNum);
	}

	/**
	 * Retorna un número en forma de Token.
	 * @param c primer caracter del operador.
	 */
	private Token GetOperatorToken(String c) {
		// guardo el operador
		String token = operators.get(c);

		//me fijo si armo un operador tipo <= o similar
		final String nc = c + this.buffer.seeWithoutConsume();
		final String nToken = operators.get(nc);
		if(nToken != null)
		{
			token = nToken;
			c += buffer.nexChar();
		}

		if (token == null){
			HandleExceptions(new InvalidOperatorException(lineNum, columNum));
			return null;
		}

		return new Token(c,token,lineNum,columNum);
	}

	/**
	 * Retorna un identificador o una palabra reservada en forma de caracter.
	 * @param c primer caracter del identificador.
	 */
	private Token GetIdentiferToken(final String c)
	{
		String lex = c;
		String tokenName;
		String tempToken;
		String nc = c;

		while(true)
		{
			if(!IsNumberOrLetter(nc) && !nc.equals("_"))
			{
				HandleExceptions(new BadIdentifierException(lineNum, columNum));
				return null;
			}

			tempToken = reserved.get(lex);
			tokenName = tempToken != null ? tempToken : "id";

			nc = buffer.seeWithoutConsume();
			if(separators.get(nc) != null || operators.get(nc) != null || buffer.EOF || nc.equals("|") ||nc.equals("&"))
			{
				break;
			}

			lex += buffer.nexChar();
		}
		
		//TODO: fix this so its more abstract
		if(lex.equals("_"))
		{
			HandleExceptions(new BadIdentifierException(lineNum, columNum));
			return null;
		}

		return new Token(lex,tokenName,lineNum,columNum);
	}

	/**
	 * Dado un string, retorna true si es un número o una letra..
	 * @param c caracter a checkear.
	 */
	private Boolean IsNumberOrLetter(String c)
	{
		char nc = c.charAt(0);
		return Character.isLetterOrDigit(nc);
	}

	/**
	 * Muestra un mensaje de error según el tipo de excepción que sea.
	 * @param e Error de tipo LexicalException.
	 */
	private void HandleExceptions(LexicalException e)
	{
		e.ShowCustomMsg();
		throw e;
	}
}
