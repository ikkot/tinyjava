package tinyjava.AnalizadorLexico.Exceptions;

/**
 * Se lanza cuando una cadena o un caracter no es cerrado correctamente.
 */
public class UnclosedLiteralException extends LexicalException
{
	private String typeOfLiteral; //!< Cambia según si el error sucedió en un string o un caracter.

    /** Constructor 
	*	@param fila - Fila en la que sucedió el error.
	*   @param colum - Columna en la que sucedió el error.
	*	@param isString - Cambia según se haya producido por un string o un caracter.
    */

	public UnclosedLiteralException(int fila, int colum,boolean isString) 
	{
		super(((isString ? "String" : "Caracter")+" sin cerrar."),fila, colum);
		typeOfLiteral = isString ? "String" : "Caracter";
		
		
        
	}

	/**
    *  Muestra un mensaje detallando el error.
    */
	@Override
	public void ShowCustomMsg()
	{
		//System.err.println(typeOfLiteral+" sin cerrar.");
	}

}
