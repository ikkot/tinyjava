package tinyjava.AnalizadorLexico.Exceptions;

public class InvalidCharacterException extends LexicalException
{
	public InvalidCharacterException(int fila, int colum) 
	{
		super("El caracter es inválido.",fila, colum);
	}

	@Override
	public void ShowCustomMsg()
	{
		//System.err.println("El caracter es inválido.");
	}

}
