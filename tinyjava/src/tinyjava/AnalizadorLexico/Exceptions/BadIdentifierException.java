package tinyjava.AnalizadorLexico.Exceptions;

public class BadIdentifierException extends LexicalException
{
	public BadIdentifierException(int fila, int colum) 
	{
		super("Identificador incorrecto.",fila, colum);
	}

	@Override
	public void ShowCustomMsg()
	{
		//System.err.println("Identificador incorrecto.");
	}

}
