package tinyjava.AnalizadorLexico.Exceptions;

public class InvalidOperatorException extends LexicalException
{
    public InvalidOperatorException(int fila, int colum) 
	{
		super("El operador esta incompleto o es invalido.",fila, colum);
	}

	@Override
	public void ShowCustomMsg()
	{
		//System.err.println("El operador esta inconpleto o es invalido.");
	}

}