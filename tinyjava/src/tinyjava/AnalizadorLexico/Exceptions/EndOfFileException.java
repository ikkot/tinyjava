package tinyjava.AnalizadorLexico.Exceptions;

public class EndOfFileException extends LexicalException
{
	public EndOfFileException(int fila, int colum) 
	{
		super("El analizador ha llegado al final del programa inesperadamente.",fila, colum);
	}

	@Override
	public void ShowCustomMsg()
	{
		//System.err.println("El analizador ha llegado al final del programa inesperadamente.");
	}

}
