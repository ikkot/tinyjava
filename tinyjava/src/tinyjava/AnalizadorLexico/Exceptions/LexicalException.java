package tinyjava.AnalizadorLexico.Exceptions;

/**
 * Esta clase es lanzada cuando ocurre un error en el analizador léxico.
 * Imprime la línea y la columna en la que sucede el error.
 */
public class LexicalException extends RuntimeException
{
    /** @param fila - Fila en la que sucedió el error.
    *   @param colum - Columna en la que sucedió el error.
    */
    public LexicalException(String msg,int fila, int colum)
    {
        super(msg+" (Ln "+Integer.toString(fila)+":Col "+Integer.toString(colum)+")");
    }

    /**
    *  Muestra un mensaje detallando el error. Otras clases harán override de este método
    * para mostrar mensajes acorde a lo que haya sucedido.
    */
    public void ShowCustomMsg()
    {
        //System.err.println("Error léxico.");
    }
}