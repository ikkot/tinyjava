package tinyjava.AnalizadorLexico.Exceptions;

public class InvalidNumberException extends LexicalException
{
    public InvalidNumberException(int fila, int colum) 
	{
		super("El número ingresado es inválido.",fila, colum);
	}

	@Override
	public void ShowCustomMsg()
	{
		//System.err.println("El número ingresado es inválido.");
	}

}