package tinyjava.AnalizadorSemantico.Types;

/**
 * Representa un tipo de dato.
 */
public abstract class Type 
{
    String name;
    String tipo;
    public Type(String name,String tipo)
    {
        this.name = name;
        this.tipo = tipo;
    }

    public String getInstanceName() {
        return name;
    }

    /**
     * Retorna el nombre del tipo. Ej: para TypeVoid retorna "void"
     * @return
     */
    public String getTypeName() {
        return this.tipo;
    }
}