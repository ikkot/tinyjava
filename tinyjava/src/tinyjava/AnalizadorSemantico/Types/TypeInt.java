package tinyjava.AnalizadorSemantico.Types;

/**
 * Tipo de dato entero.
 */
public class TypeInt extends TypePrimitiv
{
    public TypeInt(String name) 
    {
        super(name,"int");
    }  
    
}