package tinyjava.AnalizadorSemantico.Types;

public class TypeRef extends Type 
{
    // public static String nameOfType = "ref"; /* */
    public TypeRef(String name,String type)
    {
        super(name,type);
    }

    @Override
    public String getTypeName() {
        return this.name;
    }
}