package tinyjava.AnalizadorSemantico.Types;

/**
 * Tipo de dato booleano.
 */
public class TypeBoolean extends TypePrimitiv
{
    public TypeBoolean(String name) 
    {
        super(name,"boolean");
    }  
    
}