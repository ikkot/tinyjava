package tinyjava.AnalizadorSemantico.Types;

/**
 * Tipo de dato character.
 */
public class TypeChar extends TypePrimitiv
{
    public TypeChar(String name) 
    {
        super(name,"char");
    }  
    
}