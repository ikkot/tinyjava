package tinyjava.AnalizadorSemantico.Types;

/**
 * Tipo de dato String.
 */
public class TypeString extends TypePrimitiv
{
    public TypeString(String name) 
    {
        super(name,"String");
    }  
    
}