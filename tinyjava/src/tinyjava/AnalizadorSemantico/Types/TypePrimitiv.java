package tinyjava.AnalizadorSemantico.Types;

/**
 * Tipo de dato primitivo.
 */
public class TypePrimitiv extends Type 
{
    
    public TypePrimitiv(String name,String type)
    {
        super(name,type);
    }
}