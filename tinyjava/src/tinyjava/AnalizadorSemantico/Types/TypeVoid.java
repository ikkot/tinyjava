package tinyjava.AnalizadorSemantico.Types;

/**
 * Tipo de dato Void.
 */
public class TypeVoid extends Type{
    public TypeVoid()
    {
        super("void", "void");
    }
}