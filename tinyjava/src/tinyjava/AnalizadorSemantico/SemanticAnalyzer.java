package tinyjava.AnalizadorSemantico;

import java.util.ArrayList;

import tinyjava.Ejecutador;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

public class SemanticAnalyzer 
{
    private SymbolTable table;
    public SemanticAnalyzer(SymbolTable table)
    {
        this.table = table;
    }
    public void InitAnalysis()
    {
        table.second();
    }
}