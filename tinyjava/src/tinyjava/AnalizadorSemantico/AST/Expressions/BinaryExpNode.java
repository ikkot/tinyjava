package tinyjava.AnalizadorSemantico.AST.Expressions;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.Types.TypeBoolean;
import tinyjava.AnalizadorSemantico.Types.TypeInt;

import java.util.ArrayList;
import tinyjava.AnalizadorSemantico.Exceptions.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

/**
 * Esta clase representa una expresión binaria. EJ: a+b | a<b | a+b != c()-1
 */
public class BinaryExpNode extends ExpressionNode {
    ExpressionNode leftOp; //expresion izquierda del simbolo
    ExpressionNode rightOp; //expresión derecha del simbolo
    private String[] OpBool = { "opAnd", "opOr" };
    private String[] OpComp = { "opEquiv", "opDif", "opMenor", "opMayor", "opMenorIgual", "opMayorIgual" };
    private String[] OpNum = { "opSum", "opRest", "opMult", "opDiv" };

    public BinaryExpNode(Type type, Token token, ExpressionNode right, ExpressionNode left,int scope) {
        super(type, token,scope); //tipo de retorno y simbolo de operacion
        this.leftOp = left;
        this.rightOp = right;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        String aux = leftOp.checkNode(table,pClass,pMethod).getTypeName();
        if (aux.equals(rightOp.checkNode(table,pClass,pMethod).getTypeName())) 
        {
            if (matchListOp(OpBool, token.getToken())) 
            {
                if (aux.equals("boolean")) 
                {
                    this.type = new TypeBoolean("boolean");
                    return new TypeBoolean("boolean");
                }
            }
            
            if (matchListOp(OpComp, token.getToken())) 
            {
                if (aux.equals("int")) {
                    this.type = new TypeBoolean("boolean");
                    return new TypeBoolean("boolean");
                }
            }

            if (matchListOp(OpNum, token.getToken())) 
            {
                if (aux.equals("int")) 
                {
                    this.type = new TypeInt("int");
                    return new TypeInt("int");
                }
            }
            HandleExceptionsSemantic(new TypeIncompatiblesOp(token.getLineNum(), token.getColNum(),
                    leftOp.checkNode(table,pClass,pMethod).getTypeName(), 
                    rightOp.checkNode(table,pClass,pMethod).getTypeName(), token.getToken()
            ));
            return null;

        } 
        else 
        {
            HandleExceptionsSemantic(new TypeIncompatibles(token.getLineNum(), token.getColNum(),
                    leftOp.checkNode(table,pClass,pMethod).getTypeName(), 
                    rightOp.checkNode(table,pClass,pMethod).getTypeName()
            ));
            return null;
        }

    }

    @Override
    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        printListAST.add(aux  + "|--- ExpresBin Token: " + this.token.getLexicon());
        aux = aux + "    ";
        if (this.rightOp != null) {
            printListAST.add(aux+"|--- Right ");
            printListAST = this.rightOp.printAll(printListAST, aux + "    ");
        }
        if (this.leftOp != null) {
            printListAST.add(aux+"|--- Left ");
            printListAST = this.leftOp.printAll(printListAST, aux + "    ");
        }
        return printListAST;

    }

    /**
     * Retorna true si op matchea con algun elemento de la lista tiposL. Caso contario retorna false
     */
    public Boolean matchListOp(String[] tiposL, String op) {
        for (int i = 0; i < tiposL.length; i++) {
            if (op.equals(tiposL[i])) {
                return true;
            }
        }
        return false;
    }

    public ExpressionNode getLeftOP() {
        return this.leftOp;
    }

    public void setLeftOP(ExpressionNode leftOp) {
        this.leftOp = leftOp;
    }

    public ExpressionNode getRightOp() {
        return this.rightOp;
    }

    public void setRightOp(ExpressionNode rightOp) {
        this.rightOp = rightOp;
    }

    public void HandleExceptionsSemantic(SemanticException e) {
        throw e;
    }

}