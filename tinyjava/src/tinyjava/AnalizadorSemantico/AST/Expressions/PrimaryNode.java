package tinyjava.AnalizadorSemantico.AST.Expressions;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.Types.TypeVoid;
import tinyjava.AnalizadorSemantico.AST.Encadenado.Encadenado;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

import java.util.ArrayList;
/**
 * Representa un nodo de tipo Primario. Otras clases heredan de esta para generar primarios más especificos.
 * Ej: this.a
 */
public class PrimaryNode extends ExpressionNode {
    Encadenado enc;

    public PrimaryNode(Type type, Token token, Encadenado enc,int scope) {
        super(type, token,scope);
        this.enc = enc;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        if (enc == null) {
            // buscar tabla de simbolos token
            return table.searchType(pClass, pMethod, token,this.currentScope);
        }

        Type aux = this.enc.checkNode(table,table.searchType(pClass, pMethod, token,this.currentScope).getInstanceName(),token.getLexicon());
        if (aux == null) {
            return type;
        } else {
            return aux;
        }
    }


    @Override
    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        printListAST.add(aux+"|--- " + "Tipo: " + this.type.getTypeName() + " Token: " + this.token.getLexicon()+"(Scope: "+Integer.toString(this.currentScope)+")");
        aux = aux + "    ";
        if (this.enc != null) {
            printListAST.add(aux+"|--- Enc ");
            printListAST = this.enc.printAll(printListAST, aux + "    ");
        }
        return printListAST;
    }
}