package tinyjava.AnalizadorSemantico.AST.Expressions;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.Encadenado.Encadenado;
import tinyjava.AnalizadorSemantico.Exceptions.SemanticException;
import tinyjava.AnalizadorSemantico.TablaSimbolos.ClassEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;

/**
 * Representa una llamada a this. Ej: this.a
 */
public class ThisNode extends PrimaryNode {

    public ThisNode(Type type, Token id, Encadenado enc, int scope) {
        super(type, id, enc ,scope);
    }

    @Override
    public Type checkNode(SymbolTable table, String pClass, String pMethod) {

        ClassEntry thisClass = table.getClass(pClass);
        //checkeo que el encadenado sea correcto
        Type encType = enc.checkNodeThis(table, pClass, pMethod);
        
        //checkeo que el primario al que estoy encadenado exista en la clase que this referencia
        boolean isAtr = thisClass.attributeExists(enc.getToken().getLexicon());
        boolean isMethod = thisClass.methodExists(enc.getToken().getLexicon());
        if(!isAtr && !isMethod)
        {
            HandleExceptionsSemantic(new SemanticException("Error: el método o variable \""+enc.getToken().getLexicon()+"\" no existe", 
                enc.getToken().getLineNum(),
                enc.getToken().getColNum()
            ));
        }
        return encType;
    }
}