package tinyjava.AnalizadorSemantico.AST.Expressions;

import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.ClassEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.MethodEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

import java.util.ArrayList;

/**
 * Esta clase representa una Expresión.
 */
public class ExpressionNode extends ASTNode {
    Type type;
    Token token;

    public ExpressionNode(Type type, Token token,int scope) {
        this.currentScope = scope;
        this.type = type;
        this.token = token;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        return this.type;

    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        printListAST.add(aux + "Tipo: " + this.type.getTypeName() + " Token: " + this.token.getLexicon());

        return printListAST;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Token getToken() {
        return this.token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

}