package tinyjava.AnalizadorSemantico.AST.Expressions;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.Encadenado.Encadenado;
import tinyjava.AnalizadorSemantico.Exceptions.SemanticException;
import tinyjava.AnalizadorSemantico.TablaSimbolos.ClassEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.MethodEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;

public class VarNode extends PrimaryNode {

    public VarNode(Type type, Token id, Encadenado enc, int scope) {
        super(type, id, enc, scope);
    }

    @Override
    public Type checkNode(SymbolTable table, String pClass, String pMethod) {
        
        ClassEntry cls = table.getClass(pClass);
        MethodEntry met = cls.getMethod(pMethod);
        Boolean classStatic = table.classExists(token.getLexicon());

        String varName = token.getLexicon();
        Type retTipo = null;

        //primero me fijo si esta en el scope local
        if(met.variableExistsInScope(varName,this.currentScope))
        {
            retTipo = met.getVariableFromScope(varName,this.currentScope).getTipo();
        }
        //si no es local me fijo en los parametros del metodo
        else if(met.ParameterExists(varName))
        {
            retTipo = met.getParameter(varName).getTipo();
        }

        //si no es un parametro me fijo en los atributos
        else if(cls.attributeExists(varName))
        {
            retTipo = cls.getAttribute(varName).getTipo();
        }else if(token.getLexicon().equals("System"))
        {
            retTipo = null;
        }
        //si no me fijo si es un metodo estatico
        else if(classStatic)
        {
            retTipo = null;
            MethodEntry mEntry = table.getClass(token.getLexicon()).getMethod(enc.getToken().getLexicon());
            if(mEntry == null || !mEntry.isStatic())
            {
                HandleExceptionsSemantic(new SemanticException(
                    "El método "+enc.getToken().getLexicon()+ " no es estático o no existe.",
                    token.getLineNum(),
                    token.getColNum()
                ));
            }
        }
        else
        {
            HandleExceptionsSemantic(
                new SemanticException("La variable "+token.getLexicon()+ " no existe.",
                token.getLineNum(),
                token.getColNum()
            ));
        }
        
        Type encTipo = super.checkNode(table, pClass, pMethod);
        if(encTipo != null)
        {
            retTipo = encTipo;
        }
        this.type = retTipo;
        return retTipo;
    }

}