package tinyjava.AnalizadorSemantico.AST.Expressions;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.Encadenado.Encadenado;
import tinyjava.AnalizadorSemantico.Exceptions.SemanticException;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.Types.TypeRef;

/**
 * Esta clase representa un nodo de instanciación. Ej: C a = new C();
 */
public class NewNode extends PrimaryNode {

    public NewNode(Type type, Token token, Encadenado enc,int scope) {
        super(type, token, enc,scope);

    }

    @Override
    public Type checkNode(SymbolTable table, String pClass, String pMethod) {
        //controlo que lo que estoy creando exista en la tabla
        String clsName = token.getLexicon();
        
        if(clsName.equals("System"))
        {
            HandleExceptionsSemantic(new SemanticException("Error. La clase System no es instanciable.", token.getLineNum(), token.getColNum()));
        }
        
        if(!table.classExists(clsName))
        {
            HandleExceptionsSemantic(new SemanticException("Error. La clase que quiere instanciar no existe.", token.getLineNum(), token.getColNum()));
        }
        //controlo los argumentos
        //TODO
        return new TypeRef(clsName, "ref");
    }
}