package tinyjava.AnalizadorSemantico.AST.Expressions;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.Encadenado.Encadenado;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;

public class MethodNode extends PrimaryNode {

    public MethodNode(Type type, Token token, Encadenado enc,int scope) {
        super(type, token, enc,scope);
    }

    @Override
    public Type checkNode(SymbolTable table, String pClass, String pMethod) {
        return enc.checkNode(table, pClass, pMethod);
    }
    
}