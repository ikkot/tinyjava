package tinyjava.AnalizadorSemantico.AST.Expressions;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.Encadenado.Encadenado;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;
import java.util.ArrayList;

/**
 * Este nodo representa una sentencia de llamada en el ast. Ej: ( a() )
 */
public class ParenthesisNode extends PrimaryNode {
    ExpressionNode expres; //expresion a la que se llama

    public ParenthesisNode(Type type, Token token, Encadenado enc, ExpressionNode expres,int scope) {
        super(type, token, enc,scope);
        this.expres = expres;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        return this.expres.checkNode(table,pClass,pMethod);
    }

    @Override
    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        printListAST.add(aux + "|--- ParNode Tipo: " + this.type.getTypeName() + " Token: " + this.token.getLexicon());
        aux = aux + "    ";
        if (this.expres != null) {
            printListAST.add(aux+"|--- Exp ");
            printListAST = this.expres.printAll(printListAST, aux + "    ");
        }
        if (this.enc != null) {
            printListAST.add(aux+"|--- Enc ");
            printListAST = this.enc.printAll(printListAST, aux + "    ");
        }
        return printListAST;
    }
}