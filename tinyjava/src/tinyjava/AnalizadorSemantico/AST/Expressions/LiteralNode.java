package tinyjava.AnalizadorSemantico.AST.Expressions;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.Encadenado.Encadenado;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;

/**
 * Esta clase representa un valor literal. Ej: 5 | true
 */
public class LiteralNode extends PrimaryNode {

    public LiteralNode(Type type, Token id,int scope) {
        super(type, id, null,scope);
    }
    
    @Override
    public Type checkNode(SymbolTable table, String pClass, String pMethod) {
        return type;
    }
}