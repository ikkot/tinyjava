package tinyjava.AnalizadorSemantico.AST.Expressions;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;
import java.util.ArrayList;

public class UnaryExpNode extends ExpressionNode {
    ExpressionNode rightOp;

    public UnaryExpNode(Type type, Token token, ExpressionNode right,int scope) {
        super(type, token, scope);
        this.rightOp = right;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        return this.rightOp.checkNode(table,pClass,pMethod);

    }

    public ExpressionNode getRight() {
        return this.rightOp;
    }

    public void setRigt(ExpressionNode rightOp) {
        this.rightOp = rightOp;
    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        printListAST.add(aux +"|--- ExpUn "+ " Tipo: " + this.type.getTypeName() + " Token: " + this.token.getLexicon());
        aux = aux + "    ";
        if (this.rightOp != null) {
            printListAST.add(aux+"|--- Right ");
            printListAST = this.rightOp.printAll(printListAST, aux + "    ");
        }
        return printListAST;
    }
}