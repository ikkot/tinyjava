package tinyjava.AnalizadorSemantico.AST;

import tinyjava.AnalizadorSemantico.Exceptions.SemanticException;
import tinyjava.AnalizadorSemantico.TablaSimbolos.ClassEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.MethodEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;

public abstract class ASTNode {
    public int currentScope;
    public abstract Type checkNode(SymbolTable table,String pClass,String pMethod);
    
    public void HandleExceptionsSemantic(SemanticException e) {
        throw e;
    }
}