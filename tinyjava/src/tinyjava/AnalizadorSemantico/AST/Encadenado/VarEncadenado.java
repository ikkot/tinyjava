package tinyjava.AnalizadorSemantico.AST.Encadenado;

import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.Types.TypeVoid;
import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

import java.util.ArrayList;

/**
 * Llamada a una variable en un encadenado.
 */
public class VarEncadenado extends Encadenado {
    Encadenado enc;

    public VarEncadenado(Type type, Token token, Encadenado enc,int scope) {
        super(type, token,scope);
        this.enc = enc;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        
        if (enc == null) {
            // buscar tabla de simboles token
            return table.searchTypeVarEnca(pClass, pMethod, token);
        }
        
        Type aux = this.enc.checkNode(table,table.searchType(pClass, pMethod, token,this.currentScope).getInstanceName(),token.getLexicon());
        if (aux == null) {
            return type;
        } else {
            return aux;
        }

    }
    @Override
    public Type checkNodeThis(SymbolTable table,String pClass,String pMethod) {
        
        if (enc == null) {
            // buscar tabla de simboles token
            this.type = table.searchTypeThis(pClass, pMethod, token);
            return this.type;
        }

        Type aux = this.enc.checkNode(table,table.searchTypeThis(pClass, pMethod, token).getInstanceName(),token.getLexicon());
        if (aux == null) {
            return type;
        } else {
            return aux;
        }

    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Token getToken() {
        return this.token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        printListAST.add(aux + "|--- VarEn Tipo: " + this.type.getTypeName() + " Token: " + this.token.getLexicon());
        aux = aux + "    ";
        if (this.enc != null) {
            printListAST.add(aux+"|--- Enc ");
            printListAST = this.enc.printAll(printListAST, aux + "    ");
        }
        return printListAST;
    }

}