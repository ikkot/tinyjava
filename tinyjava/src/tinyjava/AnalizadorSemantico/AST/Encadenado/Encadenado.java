package tinyjava.AnalizadorSemantico.AST.Encadenado;

import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.ClassEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.MethodEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

import java.util.ArrayList;

public class Encadenado extends ASTNode {
    Type type;
    Token token;

    public Encadenado(Type type, Token token,int scope) {
        this.currentScope = scope;
        this.type = type;
        this.token = token;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        return type;
    }

    public Type checkNodeThis(SymbolTable table,String pClass,String pMethod) {
        return type;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Token getToken() {
        return this.token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        printListAST.add(aux + "|--- Enc Tipo: " + this.type.getTypeName() + " Token: " + this.token.getLexicon());
        return printListAST;
    }

}