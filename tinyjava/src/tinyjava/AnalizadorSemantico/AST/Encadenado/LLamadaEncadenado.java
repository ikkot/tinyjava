package tinyjava.AnalizadorSemantico.AST.Encadenado;

import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.Types.TypeVoid;
import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.*;
import tinyjava.AnalizadorSemantico.AST.Expressions.ExpressionNode;
import tinyjava.AnalizadorSemantico.Exceptions.ArgsNumInvalid;
import tinyjava.AnalizadorSemantico.Exceptions.TypeIncompatibles;
import tinyjava.AnalizadorSemantico.Exceptions.TypeNotFound;
import tinyjava.AnalizadorSemantico.TablaSimbolos.MethodEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

import java.util.ArrayList;

public class LLamadaEncadenado extends Encadenado {
    Encadenado enc;
    ArrayList<ExpressionNode> argumentos;
    String oClass;
    String oMethod;

    /**
     * Llamada a un metodo en un encadenado.
     */
    public LLamadaEncadenado(Type type, Token token, Encadenado enc, ArrayList<ExpressionNode> argumentos,String oClass, String oMethod,int scope) {
        super(type, token,scope);
        this.enc = enc;
        this.argumentos = argumentos;
        this.oClass = oClass;
        this.oMethod = oMethod;

    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Token getToken() {
        return this.token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        
        printListAST.add(aux + "|---CallEnc Tipo: " + this.type.getTypeName() + " Token: " + this.token.getLexicon());
        aux = aux + "    ";

        if (this.argumentos != null) {
            printListAST.add(aux+"|--- Arg ");
            for (ExpressionNode ele : this.argumentos) {
                printListAST = ele.printAll(printListAST, aux + "    ");
            }
        }
        if (this.enc != null) {
            printListAST.add(aux+"|--- Enc ");
            printListAST = this.enc.printAll(printListAST, aux + "    ");
        }
        return printListAST;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        
        pMethod = token.getLexicon();
        int contador = 0;
        if (this.argumentos != null) {
            int num = table.searchTypeArgNum(pClass, pMethod);
            if(num != this.argumentos.size() && num != -2){
                HandleExceptionsSemantic(new ArgsNumInvalid(token.getLineNum(), token.getColNum(), token.getLexicon()));
            }
            
            if( num == -2){
                Type tipo1Tem = new TypeVoid();
                Type tipo2Tem = new TypeVoid();;
                MethodEntry[] oberMet = table.searchTypeArgSobNum(pClass, pMethod);
                Boolean boolState =false;
                Boolean boolState2 =false;
                for (MethodEntry met : oberMet) {
                    contador = 0;
                    boolState =false;
                    if(this.argumentos.size()!=met.addParameterNum()){
                        boolState =true;
                        boolState2 =true;
                        break;
                      
                    }
                    
                    for (ExpressionNode ele : this.argumentos) {
                        Type tipo1 = ele.checkNode(table,oClass,oMethod);
                        Type tipo2 = table.searchTypeArgSobre(pClass, pMethod, contador,met);
                        contador++;
                        if (!tipo1.getTypeName().equals(tipo2.getTypeName())) {
                            boolState =true;
                            tipo1Tem=tipo1;
                            tipo2Tem=tipo2;
                            break;
                        }
                    }
                    if(!boolState){
                        break;
                    }
                }

                if(boolState && this.argumentos.size() != 0){
                    if(boolState2){
                        HandleExceptionsSemantic(new ArgsNumInvalid(token.getLineNum(), token.getColNum(), token.getLexicon()));
 
                    }else{
                        HandleExceptionsSemantic(new TypeIncompatibles(token.getLineNum(), token.getColNum(), tipo1Tem.getTypeName(),tipo2Tem.getTypeName()));
                        // buscar en la tabla de simbolos si coninsiden los argumentos

                    }

                                  }


            }else{
                for (ExpressionNode ele : this.argumentos) {
                    Type tipo1 = ele.checkNode(table,oClass,oMethod);
    
                    Type tipo2 = table.searchTypeArg(pClass, pMethod, contador);
                    contador++;
                
                    if (!tipo1.getTypeName().equals(tipo2.getTypeName())) {
                        HandleExceptionsSemantic(new TypeIncompatibles(token.getLineNum(), token.getColNum(), tipo1.getTypeName(),tipo2.getTypeName()));
                        // buscar en la tabla de simbolos si coninsiden los argumentos
                    }
                }
            }
            
        }

        if (enc == null) {
            // buscar tabla de simboles token
            this.type = table.searchTypeEn(pClass, pMethod, token);
            return this.type;
        }

        Type aux = this.enc.checkNode(table,table.searchType(pClass, pMethod, token,this.currentScope).getInstanceName(),token.getLexicon());
        if (aux == null) {
            return type;
        } else {
            return aux;
        }

    }
    @Override
    public Type checkNodeThis(SymbolTable table,String pClass,String pMethod) {
        return this.checkNode(table, pClass, pMethod);
    }
    

}