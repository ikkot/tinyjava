package tinyjava.AnalizadorSemantico.AST.Sentences;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.AST.Expressions.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

import java.util.ArrayList;

public class CallNode extends SentenceNode {
    PrimaryNode called;

    public CallNode(Token token, PrimaryNode called,int scope) {
        super(token,scope);
        this.called = called;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        return this.called.checkNode(table,pClass,pMethod);
    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {

        printListAST.add(aux + "|--- Call ");
        aux = aux + "    ";
        if (this.called != null) {
            printListAST = this.called.printAll(printListAST, aux);
        }
        return printListAST;
    }
}