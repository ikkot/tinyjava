package tinyjava.AnalizadorSemantico.AST.Sentences;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.Types.TypeVoid;
import tinyjava.AnalizadorSemantico.AST.Encadenado.Encadenado;
import tinyjava.AnalizadorSemantico.AST.Expressions.*;
import java.util.ArrayList;
import tinyjava.AnalizadorSemantico.Exceptions.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
public class AssignationNode extends SentenceNode{
    
    ExpressionNode exp;
    Encadenado enc;
    public AssignationNode(Token id,ExpressionNode exp,Encadenado enc,int scope)
    {
        super(id,scope);
        this.exp = exp;
        this.enc = enc;
    }
    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        Type type1;
        if(enc == null){
            //Buscar si esta definido en la tabla
            type1 = table.searchType(pClass, pMethod, token,this.currentScope);
 
        }else{
            //si es una asignacion a un this repito la clase y el metodo
            String met = this.token.getLexicon().equals("this") ? pMethod : token.getLexicon();

            if(this.token.getLexicon().equals("this")){
                type1 = this.enc.checkNodeThis(table,table.searchType(pClass, pMethod, token,this.currentScope).getInstanceName(),met);
            }else{
                type1 = this.enc.checkNode(table,table.searchType(pClass, pMethod, token,this.currentScope).getInstanceName(),met);
            }
           
        }
        Type type2= this.exp.checkNode(table,pClass,pMethod);
        if(!type1.getTypeName().equals(type2.getTypeName())){
            HandleExceptionsSemantic(new TypeIncompatibles(token.getLineNum(), token.getColNum(),
            type1.getTypeName(), type2.getTypeName()));
        }
        this.sentenceType = type2;
        return type2;
    }

    public void checkId()
    {

    }
    public ArrayList<String> printAll(ArrayList<String> printListAST,String aux)
    {
   
        printListAST.add(aux +"|--- Asig Token: "+this.token.getLexicon()+" Tipo: "+this.getSentenceType().getTypeName());
        aux = aux + "    ";
        if(this.enc != null){
            printListAST.add(aux+"|--- Enc ");
            printListAST=this.enc.printAll( printListAST,aux+"    ");
        }
        if(this.exp != null){
            printListAST.add(aux+"|--- Exp ");
            printListAST=this.exp.printAll( printListAST,aux+"    ");
        }

        return printListAST;  
    }
}