package tinyjava.AnalizadorSemantico.AST.Sentences;

import java.util.LinkedList;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.Types.TypeVoid;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;
import java.util.ArrayList;

public class BlockNode extends SentenceNode {
    LinkedList<SentenceNode> sentences;

    public BlockNode(Token token,int scope) {
        super(token,scope);
        sentences = new LinkedList<SentenceNode>();
    }

    public void addSentence(SentenceNode sent) {
        this.sentences.add(sent);
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        for (SentenceNode sent : this.sentences) 
        {
            Type s = sent.checkNode(table,pClass,pMethod);
        }
        return new TypeVoid();
    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        
        printListAST.add(aux + "|--- Block " );
        aux = aux + "    ";
        for (SentenceNode ele : this.sentences) {
            printListAST = ele.printAll(printListAST, aux );
        }
        return printListAST;
    }
}