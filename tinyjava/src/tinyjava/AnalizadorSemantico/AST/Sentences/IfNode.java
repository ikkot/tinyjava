package tinyjava.AnalizadorSemantico.AST.Sentences;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.Types.TypeVoid;
import tinyjava.AnalizadorSemantico.AST.Expressions.*;
import java.util.ArrayList;
import tinyjava.AnalizadorSemantico.Exceptions.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

public class IfNode extends SentenceNode {
    ExpressionNode condition;
    SentenceNode sent;

    public IfNode(Token token, ExpressionNode cond, SentenceNode sent,int scope) {
        super(token,scope);
        this.condition = cond;
        this.sent = sent;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) 
    {
        Boolean returnIf ;
        returnIf=table.returnState;
        Type aux = this.condition.checkNode(table,pClass,pMethod);
        if (!aux.getTypeName().equals("boolean")) 
        {
            HandleExceptionsSemantic(new TypeIncompatiblesCon(token.getLineNum(), token.getColNum(), aux.getTypeName()));
        }
        this.sent.checkNode(table,pClass,pMethod);
        table.returnState = returnIf;
        return new TypeVoid();
    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) 
    {
        
        printListAST.add(aux + "|---If ");
        aux = aux + "    ";
        if (this.condition != null) {
            printListAST.add(aux+"|--- Condicion ");
            printListAST = this.condition.printAll(printListAST, aux + "-C-  ");
        }
        if (this.sent != null) {
            printListAST.add(aux+"|--- Then ");
            printListAST = this.sent.printAll(printListAST, aux + "-S-  ");
        }
        return printListAST;
    }
}