package tinyjava.AnalizadorSemantico.AST.Sentences;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.Types.TypeVoid;
import tinyjava.AnalizadorSemantico.AST.Expressions.*;
import java.util.ArrayList;
import tinyjava.AnalizadorSemantico.Exceptions.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

public class WhileNode extends SentenceNode {
    ExpressionNode condition;
    SentenceNode sent;

    public WhileNode(Token token, ExpressionNode condition, SentenceNode sent,int scope) {
        super(token,scope);
        this.condition = condition;
        this.sent = sent;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        Type aux = this.condition.checkNode(table,pClass,pMethod);
        
        if (!aux.getTypeName().equals("boolean")) {
            HandleExceptionsSemantic(new TypeIncompatiblesCon(token.getLineNum(), token.getColNum(), aux.getTypeName()));
        }
        this.sent.checkNode(table,pClass,pMethod);
        return new TypeVoid();
    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {

        printListAST.add(aux + "|---While ");
        aux = aux + "    ";
        if (this.condition != null) {
            printListAST.add(aux+"|--- Condicion ");
            printListAST = this.condition.printAll(printListAST, aux + "    ");
        }
        if (this.sent != null) {
            printListAST.add(aux+"|--- Then ");
            printListAST = this.sent.printAll(printListAST, aux + "    ");
        }
        return printListAST;
    }
}