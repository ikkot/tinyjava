package tinyjava.AnalizadorSemantico.AST.Sentences;

import java.util.ArrayList;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.Expressions.ExpressionNode;
import tinyjava.AnalizadorSemantico.Exceptions.SemanticException;
import tinyjava.AnalizadorSemantico.Exceptions.TypeIncompatiblesCon;
import tinyjava.AnalizadorSemantico.TablaSimbolos.MethodEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.Types.TypeVoid;

public class EmptyRtnNode extends ReturnNode {

    public EmptyRtnNode(Token token,int scope) {
        super(token, null, null,scope);
    }

    @Override
    public Type checkNode(SymbolTable table, String pClass, String pMethod) 
    {
        MethodEntry met = table.getClass(pClass).getMethod(pMethod);
        if(!met.getTipo().getTypeName().equals("void"))
        {
            //TODO: error
            HandleExceptionsSemantic(new SemanticException("Falta el valor de retorno.",
                this.token.getLineNum(),
                this.token.getColNum()
            ));
        }
        return new TypeVoid();
    }

    @Override
    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        aux = aux + "|--- Return ";
        printListAST.add(aux);
        aux = aux + "    ";
        if (this.rNode != null) {
            printListAST = this.rNode.printAll(printListAST, aux);
        }
        return printListAST;

    }
    
}