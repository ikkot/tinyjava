package tinyjava.AnalizadorSemantico.AST.Sentences;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.AST.Expressions.ExpressionNode;
import tinyjava.AnalizadorSemantico.Exceptions.IncompatibleTypes;
import tinyjava.AnalizadorSemantico.Exceptions.ReturnVoid;
import tinyjava.AnalizadorSemantico.Exceptions.TypeIncompatibles;
import tinyjava.AnalizadorSemantico.TablaSimbolos.ClassEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.MethodEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSemantico.Types.Type;
import java.util.ArrayList;

public class ReturnNode extends SentenceNode {

    Type type;
    ExpressionNode rNode;

    public ReturnNode(Token token, Type retType, ExpressionNode retExp,int scope) {
        super(token,scope);
        type = retType;
        rNode = retExp;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) 
    {
        table.returnState = true;
        MethodEntry mEntry = table.getClass(pClass).getMethod(pMethod); 
        String tipo1 = mEntry.getTipo().getTypeName();
        String tipo2 = rNode.checkNode(table, pClass, pMethod).getTypeName(); 
        if(!tipo1.equals(tipo2))
        {
            HandleExceptionsSemantic(new TypeIncompatibles(token.getLineNum(), token.getColNum(),tipo1,tipo2));
        }
        if(tipo2.equals("void")){
            HandleExceptionsSemantic(new ReturnVoid(token.getLineNum(), token.getColNum()));
        }
        this.sentenceType = type;
        return type;
    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        
        printListAST.add(aux + "|--- Return Tipo: " + this.getSentenceType().getTypeName() );
        aux = aux + "    ";
        if (this.rNode != null) {
            printListAST = this.rNode.printAll(printListAST, aux);
        }
        return printListAST;
    }
}