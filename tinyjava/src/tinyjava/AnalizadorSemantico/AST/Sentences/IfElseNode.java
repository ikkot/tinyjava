package tinyjava.AnalizadorSemantico.AST.Sentences;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.Types.TypeVoid;
import tinyjava.AnalizadorSemantico.AST.Expressions.*;
import java.util.ArrayList;
import tinyjava.AnalizadorSemantico.Exceptions.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

public class IfElseNode extends IfNode {

    SentenceNode sentElse;

    public IfElseNode(Token token, ExpressionNode cond, SentenceNode sent, SentenceNode sentElse,int scope) {
        super(token, cond, sent,scope);
        this.sentElse = sentElse;
    }

    @Override
    public Type checkNode(SymbolTable table,String pClass,String pMethod) {
        Boolean returnIf = false;
        Boolean returnIfSen1 = false;
        Boolean returnIfSen2 = false;
        Type aux = this.condition.checkNode(table,pClass,pMethod);
        if (!aux.getTypeName().equals("boolean")) {
            HandleExceptionsSemantic(new TypeIncompatiblesCon(token.getLineNum(), token.getColNum(), aux.getTypeName()));
        }

        returnIf=table.returnState;
        table.returnState=false;
        this.sent.checkNode(table,pClass,pMethod);
        returnIfSen1=table.returnState;
        table.returnState=false;
        this.sentElse.checkNode(table,pClass,pMethod);
        returnIfSen2=table.returnState;
        table.returnState=false;

        if(returnIf){
            table.returnState=true;
        }else{
            if(returnIfSen1){
                if(returnIfSen2){
                    table.returnState=true;
                }else{
                    table.returnState=false;
                }
            }else{
                table.returnState=false;
            }
        }
        return new TypeVoid();
    }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
    
        printListAST.add(aux + "|--- If " );
        aux = aux + "    ";
        if (this.condition != null) {
            printListAST.add(aux+"|--- Condicion ");
            printListAST = this.condition.printAll(printListAST, aux + "    ");
        }
        if (this.sent != null) {
            printListAST.add(aux+"|--- Then ");
            printListAST = this.sent.printAll(printListAST, aux + "    ");
        }
        if (this.sentElse != null) {
            printListAST.add(aux+"|--- Else ");
            printListAST = this.sent.printAll(printListAST, aux + "    ");
        }
        return printListAST;
    }
}