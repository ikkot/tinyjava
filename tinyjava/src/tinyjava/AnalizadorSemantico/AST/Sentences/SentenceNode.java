package tinyjava.AnalizadorSemantico.AST.Sentences;

import tinyjava.Token;
import tinyjava.AnalizadorSemantico.Types.Type;
import tinyjava.AnalizadorSemantico.AST.ASTNode;
import java.util.ArrayList;
import tinyjava.AnalizadorSemantico.Exceptions.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.ClassEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.MethodEntry;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;

public class SentenceNode extends ASTNode {

    Token token;
    Type sentenceType;

    public SentenceNode(Token token,int scope) {
        this.token = token;
        this.currentScope = scope;
    }

    public Type getSentenceType() {
        return sentenceType;
    }

    public void setSentenceType(Type sentenceType) {
        this.sentenceType = sentenceType;
    }


    // @Override
    // public Type checkNode() {
    //     return this.sentenceType;

    // }

    public ArrayList<String> printAll(ArrayList<String> printListAST, String aux) {
        if(token == null || sentenceType == null){
            return printListAST;
        }
        printListAST.add(aux + "|--- Sent Tipo: " + this.sentenceType.getTypeName() + " Token: " + this.token.getLexicon());
        return printListAST;
    }


    @Override
    public Type checkNode(SymbolTable table, String pClass, String pMethod) {
        // TODO Auto-generated method stub
        return null;
    }
}