package tinyjava.AnalizadorSemantico.TablaSimbolos;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Esta clase representa un scope local. 
 * Un scope local es un conjunto de variables solo utilizables en ese espacio.
 * Tambien contiene una referencia al scope padre.
 */
public class LocalScope {

    LocalScope parentScope; //scope padre. hereda sus variables
    LinkedHashMap<String,VariableEntry> scope; //variables pertenecientes al scope
    private int scopeName; //nombre del scope

    public LocalScope(LocalScope parnt, int name)
    {
        parentScope = parnt;
        scope = new LinkedHashMap<String,VariableEntry>();
        scopeName = name;
    }

    public LocalScope(int name)
    {
        scopeName = name;
        scope = new LinkedHashMap<String,VariableEntry>();
    }

    public boolean variableExists(String name)
    {
        if(parentScope != null)
            return parentScope.variableExists(name) || scope.containsKey(name);
        else
            return scope.containsKey(name);
    }

    public void addVariable(VariableEntry nVar)
    {
        scope.put(nVar.getName(),nVar);
    }

    public int getScopeName()
    {
        return scopeName;
    }

    public VariableEntry getVariable(String name)
    {
        VariableEntry vEntry = scope.get(name);
        //si no está en el scope local busco en el padre
        if(vEntry == null && parentScope != null)
        {
            vEntry = parentScope.getVariable(name);
        }
        return vEntry;
    }

    public void printScope()
    {
        if(scope.size() == 0) return;
        System.out.println("#---- Scope "+Integer.toString(scopeName)+"----#");
        if(parentScope != null)
            System.out.println("Scope padre: "+parentScope.scopeName);
        for (VariableEntry vars : scope.values()) 
        {
            System.out.println(vars.getTipo().getTypeName()+ " "+vars.getName());
        }
        System.out.println("-------");
    }

    public ArrayList<String> printScopeAll(ArrayList<String> printListEDT) 
    {
        for (VariableEntry variable : scope.values()) {
            printListEDT = variable.printVariable(printListEDT,scopeName);
        }
        return printListEDT;
    }
}