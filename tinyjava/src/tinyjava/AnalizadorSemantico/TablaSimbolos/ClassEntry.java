package tinyjava.AnalizadorSemantico.TablaSimbolos;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import tinyjava.AnalizadorSemantico.Exceptions.*;

public class ClassEntry extends TableEntry 
{
    private String parent; /*<Nombre de la clase padre. */ 
    private ClassEntry parentObj; //objeto padre
    private CtorEntry constructor;
    LinkedHashMap<String,AttributeEntry> variablesTable; //lista de atributos
    LinkedHashMap<String,MethodEntry> methodsTable; //lista de metodos

    public ClassEntry(String name,int lineNumber,int rowNumber,SymbolTable table)
    {
        super(name,lineNumber,rowNumber,table);
        variablesTable = new LinkedHashMap<String,AttributeEntry>();
        methodsTable = new LinkedHashMap<String,MethodEntry>();
        this.constructor = null;
    }

    public void inheritsFrom(String parent) 
    {
        this.parent = parent;
    }

    public void setParentObj(ClassEntry parent)
    {
        this.parentObj = parent;
    }

    public void setParentFromTable()
    {
        this.parentObj = symbolTable.getClass(this.parent);
    }

    //#region Metodos
    public void addMethod(MethodEntry m)
    {
        methodsTable.put(m.getName(),m);
    }

    public void addInheritedMethods()
    {
        ArrayList<MethodEntry> mtods = parentObj.getMethodsList();
        for (MethodEntry methodEntry : mtods) {
            if(methodsTable.containsKey(methodEntry.getName()))
            {
                HandleExceptionsSemantic(new NonExistingClass(methodEntry.getLineNumber(), methodEntry.getColNumber(),methodEntry.getName()));
                //error: un metodo heredado y un metodo local no pueden tener el mismo nombre
                return;
            }
            else
            {
                methodsTable.put(methodEntry.getName(), methodEntry);
            }
        }
    }

    public boolean methodExists(String m)
    {
        return methodsTable.containsKey(m);
    }
    
    public ArrayList<MethodEntry> getMethodsList()
    {
        return new ArrayList<MethodEntry>(methodsTable.values());
    }

    public MethodEntry getMethod(String name)
    {
        return methodsTable.get(name);
    }

    public void addConstructor(CtorEntry m)
    {
        this.constructor = m;
    }

    public CtorEntry getConstructor()
    {
        return this.constructor;
    }

    public boolean hasConstructor()
    {
        return constructor != null;
    }
    //#endregion

    public String getParent() 
    {
        return parent;
    }
    
    //#region Variables
    public void addAttribute(AttributeEntry vEntry)
    {
        variablesTable.put(vEntry.getName(),vEntry);
    }

    public ArrayList<AttributeEntry> getVariablesList()
    {
        return new ArrayList<AttributeEntry>(variablesTable.values());
    }

    public boolean attributeExists(String v)
    {
        return variablesTable.containsKey(v);
    }
    //#endregion

    @Override
    public ArrayList<String> printEntry(ArrayList<String> printListEDT) {
        String aux = "";
        String aux2 = "";
        for(int i=0;i<=80;i++){
            aux+="-";
        }
        for(int i=0;i<=(80-this.getName().length()-9)/2;i++){
            aux2+="-";
        }

        printListEDT.add(aux);
        printListEDT.add(aux2+" Clase "+this.getName()+" "+aux2);
        printListEDT.add(aux);

        printListEDT.add("");
        printListEDT.add("    Padre: "+this.parent);
        printListEDT.add("");
        printListEDT.add("    #-Atributos-#");
        for (AttributeEntry iter : variablesTable.values()) {
            printListEDT = iter.printEntry(printListEDT);
            
        }
        if(hasConstructor())
        {
            printListEDT.add("");
            printListEDT.add("    #-Constructor-#");
            printListEDT = this.constructor.printEntry(printListEDT);    
        }
        printListEDT.add("");
        printListEDT.add("    #-Metodos-#");
        for (MethodEntry iter : methodsTable.values())
        {
            printListEDT = iter.printEntry(printListEDT);
        }
        printListEDT.add("");
        return printListEDT;
    }

    public ClassEntry getParentObj() 
    {
        return parentObj;
    }
    public void HandleExceptionsSemantic(SemanticException e)
	{
		throw e;
    }
    
    public void CheckClass()
    {
        for (var method : methodsTable.values()) {
            method.checkMethod(this);
        }

    }
    public ArrayList<String> CheckClassPrint(ArrayList<String> printListAST)
    {
        String aux = "";
        String aux2 = "";
        for(int i=0;i<=80;i++){
            aux+="-";
        }
        for(int i=0;i<=(80-this.getName().length()-9)/2;i++){
            aux2+="-";
        }

        printListAST.add("");
        printListAST.add("");
        printListAST.add(aux);
        printListAST.add(aux2+" Clase "+this.getName()+" "+aux2);
        printListAST.add(aux);
        printListAST.add("");

        for (var method : methodsTable.values()) {
            String aux3 = "";
            for(int i=0;i<=(60-method.getName().length()-10)/2;i++){
                aux3+="-";
            }
            printListAST.add("");
            printListAST.add(aux3+" Metodo "+method.getName()+" "+aux3);
            printListAST.add("");
            printListAST = method.checkMethodPrint(printListAST);
        }
        return printListAST;
    }

    public VariableEntry getAttribute(String name)
    {
        return variablesTable.get(name);
    }
}