package tinyjava.AnalizadorSemantico.TablaSimbolos;

import tinyjava.AnalizadorSemantico.Types.*;

/**
 * Esta clase se representa la clase System de tinyjava.
 */
public class SystemClassEntry extends ClassEntry
{

    public SystemClassEntry(SymbolTable table) 
    {
        super("System", 0, 0, table);
        Initialize();
    }

    /**
     * Añade los metodos read,print y println a la clase System
     */
    private void Initialize()
    {
        //read
        MethodEntry read = new MethodEntry("read", 0, 0, this.symbolTable, "static", new TypeInt("int"));
        this.addMethod(read);
        //print
        MethodEntry print = new SystemMethodEntry(this.symbolTable);
        this.addMethod(print);
        //println
        MethodEntry println = new MethodEntry("println", 0, 0, this.symbolTable, "static",new TypeVoid());
        this.addMethod(println);
    }    
}