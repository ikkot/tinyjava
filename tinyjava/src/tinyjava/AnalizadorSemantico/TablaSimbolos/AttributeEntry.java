package tinyjava.AnalizadorSemantico.TablaSimbolos;

import java.util.ArrayList;

import tinyjava.AnalizadorSemantico.Types.Type;
/**
 * Representa un atributo.
 */
public class AttributeEntry extends VariableEntry
{
    public String visibility; /*< Visibilidad del atributo. Puede ser public o private */
    
    public AttributeEntry(String name,int row,int column,SymbolTable table,Type type,String vis) 
    {
        super(name,row,column,table,type);
        this.visibility = vis;
    }   
    public String  getVisibility(){
        return this.visibility ;
    }    

    @Override
    public ArrayList<String> printVariable(ArrayList<String> printListEDT) {
        printListEDT.add("        "+visibility+" "+this.getTipo().getInstanceName()+" "+this.getName());
        return printListEDT;
    }
}