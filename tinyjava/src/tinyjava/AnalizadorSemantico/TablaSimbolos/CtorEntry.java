package tinyjava.AnalizadorSemantico.TablaSimbolos;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class CtorEntry extends MethodEntry
{
    public CtorEntry(String name, int row,int column,SymbolTable table)
    {
        super(name, row, column, table,null,null);
    }

    @Override
    public ArrayList<String> printEntry(ArrayList<String> printListEDT) 
    {
        printListEDT.add("    Parametros: ");
        for (VariableEntry iter : parametersList.values()) 
        {
            printListEDT = iter.printVariable(printListEDT);
        }
        return printListEDT;
    }
}