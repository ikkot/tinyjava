package tinyjava.AnalizadorSemantico.TablaSimbolos;
import tinyjava.AnalizadorSemantico.AST.Expressions.ExpressionNode;
import tinyjava.AnalizadorSemantico.Exceptions.*;
import tinyjava.AnalizadorSemantico.Types.*;
import tinyjava.Ejecutador;
import tinyjava.Token;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;



/**
 * Esta clase representa la tabla de símbolos.
 */
public class SymbolTable 
{
    private LinkedHashMap<String, ClassEntry> classes;
    private ClassEntry currentClass;
    private MethodEntry currentMethod;
    public Boolean returnState ;

    /**
     * Constructor tabla de símbolos.
     */
    public SymbolTable()
    {
        classes = new LinkedHashMap<String,ClassEntry>();
        addObjectClass();
        addSystemClass();
        this.returnState = false;
    }

    /**
     * Añade la clase Object a la tabla.
     */
    private void addObjectClass()
    {
        ClassEntry cEntry = new ClassEntry("Object", 0, 0, this);
        addClass(cEntry);
    }


    /**
     * Añade la clase System a la tabla.
     */
    private void addSystemClass()
    {
        ClassEntry cEntry = new SystemClassEntry(this);
        cEntry.inheritsFrom("Object");
        addClass(cEntry);
    }

    /**
     * Añade una clase a la tabla.
     * @param nClass la clase que se añade.
     */
    
    public void addClass(ClassEntry nClass)
    {
        classes.put(nClass.getName(), nClass);
    }

    /**
     * Añade la clase actual a la tabla.
     */
    public void addCurrentClass()
    {
        classes.put(currentClass.getName(), currentClass);
    }

    /**
     * Retorna una clase de la tabla.
     * @param name nombre de la clase que se busca en la tabla.
     * @return el objeto ClassEntry que matchea con el nombre.
     */
    public ClassEntry getClass(String name)
    {
        if(!classExists(name))
        {
            HandleExceptions(new NonExistingClass(currentClass.getLineNumber(), currentClass.getColNumber(),name));
            //error, la clase no esta declarada aun o no existe
            return null;
        }
        return classes.get(name);
    }

    /**
     * Retorna verdadero si la clase ingresada está en la tabla. Caso contrario retorna falso. 
     */
    public boolean classExists(String name)
    {
        return classes.containsKey(name);
    }

    /**
     * retorna la clase actual con la que se está trabajando
     */
    public ClassEntry getCurrentClass() {
        return currentClass;
    }

    /**
     * Setea la clase actual con el que se está trabajando 
     */
    public void setCurrentClass(ClassEntry currentClass) {
        this.currentClass = currentClass;
    }

    /**
     * Retorna el método actual con el que se esta trabajando.
     */
    public MethodEntry getCurrentMethod() {
        return currentMethod;
    }

    /**
     * Setea el metodo actual con el que se está trabajando .
     * @param currentMethod
     */
    public void setCurrentMethod(MethodEntry currentMethod) {
        this.currentMethod = currentMethod;
    }

    /**
     * Busca si una clase presenta Herencia Circular.
     */
    public Boolean circular(ClassEntry classAct) {
        ClassEntry classActTem = classAct;

        if(classActTem.getParent().equals("Object")){
            return true;
        }else{
        while(true){

            if(classActTem.getName().equals("Object")) return true;

            if (classActTem.getParent().equals(classAct.getName()) ){
                HandleExceptions(new CircularInheritance(classAct.getLineNumber(), classAct.getColNumber(),classAct.getName() ));
                return true;
            }else{
                classActTem=classes.get(classActTem.getParent()) ;
                if(classActTem==null||classAct.getName().equals("Object")){
                    return false;
                }
            }
        }}
    }

    /**
     * Checkea que los retornos de todos los métodos existan.
     */
    public void CheckMethodReturnType()
    {
        for (ClassEntry iter : classes.values()) 
        {
            if(iter.getName().equals("System")) continue;
            for(MethodEntry m : iter.getMethodsList())
            {
                checkTipeMethod(m);
            }
        }
    }

    /**
     * Checkea si el tipo de retorno de un método existe.
     */
    public Boolean checkTipeMethod(MethodEntry metAct) {
        Type tipoAct = metAct.getTipo();
        String tipoString = tipoAct.getInstanceName();
        if(classes.containsKey(tipoString)||tipoString.equals("char")||tipoString.equals("boolean")||tipoString.equals("int")||tipoString.equals("String")||tipoString.equals("void")){
            return true;
        }else {
            tipoString = tipoAct.getInstanceName();
            if(classes.containsKey(tipoString)){
                return true;
            }else{
                HandleExceptions(new NonExistentType(metAct.getLineNumber(), metAct.getColNumber(),metAct.getName()));
                return false;
            }
        }        
    }

    /**
     * Checkea si los tipos de los atributos declarados existen.
     */
    public void checkAttributes()
    {
        for (ClassEntry iter : classes.values()) {
            if(iter.getName().equals("System")) continue;
            for(VariableEntry v: iter.getVariablesList())
            {
                checkTipeVar(v);
            }
        }
    }

    /**
     * Checkea si el tipo de una variable existe.
     * @param varAct variable en la que se trabaja.
     */
    public Boolean checkTipeVar(VariableEntry varAct) {
        Type tipoAct = varAct.getTipo();
        String tipoString = tipoAct.getTypeName();
        if(tipoString=="char"||tipoString=="boolean"||tipoString=="int"||tipoString=="String"||tipoString=="void"||tipoString=="primitiv"){
            return true;
        }else {
            tipoString = tipoAct.getInstanceName();
            if(classes.containsKey(tipoString)){
                return true;
            }else{
            HandleExceptions(new NonExistentType(varAct.getLineNumber(), varAct.getColNumber(),varAct.getName()));
            return false;}
        }        
    }

    /**
     * Lanza una excepción de tipo semántica.
     * @param e
     */
    private void HandleExceptions(SemanticException e)
	{
		throw e;
	}

    /**
     * Imprime las clases de la tabla, junto con sus métodos y atributos.
     */
    public ArrayList<String> printClasses(ArrayList<String> printListEDT) 
    {
        for (ClassEntry iter : classes.values()) {
            printListEDT = iter.printEntry(printListEDT);
            //printListEDT.add("----------------");
        }
        return printListEDT;
    }


    /**
     * Para cada clase, checkea que su padre exista 
     */
    private void CheckClassInheritance()
    {
        String parent;
        for (ClassEntry iter : classes.values()) 
        {
            if(iter.getName().equals("Object") || (iter.getName().equals("System"))) continue;

            parent = iter.getParent();
            if(!parent.equals("Object") && !classExists(parent))
            {
                HandleExceptions(new ParentDeclaredException(iter.getLineNumber(), iter.getColNumber(), parent));
            }
        }
    }
    
 
    /**
     * consolida los metodos de una clase y corrobora que la signatura corresponda con los heredados.
     * @param c Clase a la que se le añaden los metodos.
     */
    private void addInheritedMethods(ClassEntry c) {
        String parentTypeName;

        if (c.getName().equals("Object"))
            return;
        if (!c.getParent().equals("Object")) { // si es Object no hay nada que consolidar
            Boolean state = true;
            ArrayList<MethodEntry> mtodsP = classes.get(c.getParent()).getMethodsList(); //metodos del padre
            ArrayList<MethodEntry> mtodsH = c.getMethodsList(); //metodos de la clase actual
            for (MethodEntry methodEntryP : mtodsP) {
                for (MethodEntry methodEntryH : mtodsH) {
                    
                    if (methodEntryH.getName().equals(methodEntryP.getName())) {// si el metodo padre e hijo tiene mismo nombre                                                     // nombre
                        parentTypeName = methodEntryP.getTipo().getInstanceName();
                        //error: el tipo de retorno es distinto
                        if(!methodEntryH.getTipo().getInstanceName().equals(parentTypeName))
                        {
                            HandleExceptions(new SignatureException(methodEntryH.getLineNumber(),
                                methodEntryH.getColNumber(), 
                                methodEntryH.getName()
                            ));
                            break;
                        }
                        
                        LinkedHashMap<String, VariableEntry> varMeH = methodEntryH.getParametersList(); //variables metodo del hijo
                        LinkedHashMap<String, VariableEntry> varMeP = methodEntryP.getParametersList(); //variables metodo del padre
                        Iterator<Map.Entry<String, VariableEntry>> varMePeT = varMeP.entrySet().iterator();
                        while (varMePeT.hasNext()) {
                            Map.Entry<String, VariableEntry> varMePFin = varMePeT.next();
                            if (varMeH.containsKey(varMePFin.getValue().getName())) {
                                if (!varMeH.get(varMePFin.getValue().getName()).getTipo().getTypeName()
                                        .equals(varMePFin.getValue().getTipo().getTypeName())) {
                                    // error : diferentes tipos entre padre e hijo
                                    HandleExceptions(new SignatureException(methodEntryH.getLineNumber(),
                                            methodEntryH.getColNumber(), methodEntryH.getName()));
                                    break;
                                }
                            } else {
                                // error: diferente cantidad de argumentos
                                HandleExceptions(new SignatureException(methodEntryH.getLineNumber(),
                                        methodEntryH.getColNumber(), methodEntryH.getName()));
                                break;
                            }
                        }
                    }
                }
                if (state)
                    c.methodsTable.put(methodEntryP.getName(), methodEntryP); // agrego metodo al hijo
            }
        }
    }

    @Deprecated
    private void addInheritedVariables(ClassEntry cEntry)
    {
        ClassEntry parent = classes.get(cEntry.getParent());
        while(!parent.getName().equals("Object"))
        {
            for (AttributeEntry variable : parent.getVariablesList()) 
            {
                if(cEntry.attributeExists(variable.getName()))
                {
                    HandleExceptions(new VariableDeclaredException(variable.getLineNumber(), variable.getColNumber(), variable.getName()));
                }
                cEntry.addAttribute(variable);
            }
            parent = classes.get(parent.getParent());
        }
    }

    /**
     * Consolida los atributos de una clase.
     * @param cEntry Clase en la que se trabaja.
     */
    private void addInheritedVariables2(ClassEntry cEntry)
    {
        LinkedList<AttributeEntry> atrList = getAttributesFromParents(cEntry);
        for (AttributeEntry attributeEntry : atrList) 
        {
            if(cEntry.attributeExists(attributeEntry.getName()))
            {
                HandleExceptions(new VariableDeclaredException(attributeEntry.getLineNumber(), attributeEntry.getColNumber(), attributeEntry.getName()));
            }
            if(attributeEntry.visibility.equals("public"))
                cEntry.addAttribute(attributeEntry);
        } 
    }

    /**
     * obtiene los atributos heredados recursivamente y los retorna en una lista.
     * @param cEntry clase con la que se trabaja.
     * @return lista de atributos.
     */
    private LinkedList<AttributeEntry> getAttributesFromParents(ClassEntry cEntry)
    {
        LinkedList<AttributeEntry> atrList = new LinkedList<AttributeEntry>();
        LinkedList<AttributeEntry> rta = gettingRecAttributes(atrList, classes.get(cEntry.getParent()));
        return rta;
    }

    /**
     * lleva a cabo la operación de recursión para atributos heredados.
     */    
    private LinkedList<AttributeEntry> gettingRecAttributes(LinkedList<AttributeEntry> atrList,ClassEntry parent)
    {
        if(parent.getName().equals("Object"))
        {
            return atrList;
        }
        
        for (AttributeEntry variable : parent.getVariablesList())
        {
            if(!atrList.contains(variable))
                atrList.add(variable);
        }
        parent = classes.get(parent.getParent());
        return gettingRecAttributes(atrList, parent);
    }

    /**
     * Efectua las tareas que el analizador semántico realiza durante la segunda pasada.
     */
    public void second()
	{
        Iterator<Map.Entry<String, ClassEntry>> objT = classes.entrySet().iterator();
        //checkeo que los padres existan
        CheckClassInheritance();
        while (objT.hasNext()) 
        {
            Map.Entry<String, ClassEntry> obj = objT.next();
            //checkeo herencia circular
            if(obj.getValue().getName().equals("Object")) continue;
            circular(obj.getValue());

            ArrayList<MethodEntry> metTem = obj.getValue().getMethodsList();
            for (MethodEntry metAct : metTem) 
            {
                //checkeo que los retornos de los metodos existen
                checkTipeMethod(metAct);
                
                //checkeo que los tipos de los parametros de un metodo existen
                LinkedHashMap<String,VariableEntry> parameteList = metAct.getParametersList();
                Iterator<Map.Entry<String, VariableEntry>> parameterT = parameteList.entrySet().iterator();
                while (parameterT.hasNext()) 
                {
                    Map.Entry<String, VariableEntry> parameter = parameterT.next();
                    checkTipeVar(parameter.getValue());
                }
            }
            
            //checkeo que los tipos de los atributos existen
            ArrayList<AttributeEntry> varTem = obj.getValue().getVariablesList();
            for (AttributeEntry varAct : varTem) 
            {
                checkTipeVar(varAct);
            }            

            //se añaden los metodos heredados
            for (ClassEntry cls : classes.values()) 
            {
                addInheritedMethods(cls);
            }
        }

        //se añaden los atributos heredados
        for (ClassEntry cls : classes.values()) 
        {
            if(cls.getName().equals("Object")) continue;

            addInheritedVariables2(cls);
        }
        //se valida el AST
        validateAST();
    }
    
    /**
     * Valido el AST creado.
     * @return void.
     */
    private void validateAST()
    {
        for (ClassEntry cls : classes.values()) {
            if(cls.getName().equals("Object") || cls.getName().equals("System")) continue;
            cls.CheckClass();
            
        }

    }
    public ArrayList<String> printAST(ArrayList<String> printListAST)
    {
        for (ClassEntry cls : classes.values()) {
            if(cls.getName().equals("Object") || cls.getName().equals("System")) continue;
            printListAST=cls.CheckClassPrint(printListAST);
        }
        return printListAST;

    }

    private Type searchMethodType(ClassEntry classTem, Token token)
    {
        //finalmente me fijo si es un metodo
        MethodEntry isMethd = classTem.methodsTable.get(token.getLexicon());
        if(isMethd != null)
        {
            return isMethd.getTipo();
        }else{
            return null;
        }
    }

    /**
     * Busca un atributo en una clase y retorna el tipo.
     * @return
     */
    private Type searchAttributeType(ClassEntry classTem,Token token)
    {
        AttributeEntry atributeTem = classTem.variablesTable.get(token.getLexicon());
        //busco en los atributos
        if( atributeTem != null){
            return atributeTem.getTipo();
        } else
        {
            return null;
        }
    }

    public Type searchTypeThis(String pClass,String pMethod, Token token)
    {
        //busco el token entre los atributos
        ClassEntry cEntry = classes.get(pClass);
        Type atrType = searchAttributeType(cEntry, token);
        if(atrType != null)
        {
            return atrType;
        }
        Type methodType = searchMethodType(cEntry, token);
        if(methodType != null)
        {
            return methodType;
        }

        HandleExceptionsSemantic(new SemanticException(token.getLexicon()+" no existe en el contexto de "+pClass,
            token.getLineNum(), 
            token.getColNum()
        ));

        return null;


    }

    /**
     * Busca el tipo de una variable en su respectiva intancia.
     * @param pClass clase padre.
     * @param pMethod metodo padre.
     * @param token token actual.
     */
    public Type searchType(String pClass,String pMethod, Token token,int scope)
    {
        ClassEntry classTem = classes.get(pClass);
        if(classTem == null)
        {
            HandleExceptionsSemantic(
                new SemanticException("La clase "+pClass+" no existe.",
                token.getLineNum(), 
                token.getLineNum())
                );
            }
            
        if(token.getLexicon().equals("this")) return new TypeRef(classTem.getName(),"ref");
        ClassEntry classStatic = classes.get(token.getLexicon());
        
        if(classStatic != null){
            return new TypeRef(token.getLexicon(),"ref");
        }
        
        if(token.getLexicon().equals("System")) return new TypeRef("System","ref");
        
        MethodEntry metodTem = classTem.methodsTable.get(pMethod);
        //Busco en las variables locales del metodo pMethod
        if( metodTem != null)
        {
            VariableEntry varLocolTem = metodTem.getVariableFromScope(token.getLexicon(),scope);
            VariableEntry varParTem = metodTem.parametersList.get(token.getLexicon());
        
            if( varLocolTem != null)
            {
                return varLocolTem.getTipo();
            }else if(varParTem != null)
            {
                return varParTem.getTipo();
            }
        }

        //busco en los atributos
        AttributeEntry atributeTem = classTem.variablesTable.get(token.getLexicon());
        if( atributeTem != null)
        {
            return atributeTem.getTipo();
        }

        //finalmente me fijo si es un metodo
        MethodEntry isMethd = classTem.methodsTable.get(token.getLexicon());
        if(isMethd != null)
        {
            return isMethd.getTipo();
        }

        HandleExceptionsSemantic(new TypeNotFound(token.getLineNum(), token.getColNum(), token.getLexicon()));
        return null;
    }
    /**
     * Busca el tipo de una variable encadenada en su respectiva intancia
     * @param pClass clase padre.
     * @param pMethod metodo padre.
     * @param token token actale.
     * @return typo .
     */
    public Type searchTypeVarEnca(String pClass,String pMethod, Token token)
    {
        ClassEntry classTem = classes.get(pClass);
        if(classTem == null)
        {
            HandleExceptionsSemantic(
                new SemanticException("La clase "+pClass+" no existe.",
                token.getLineNum(), 
                token.getLineNum())
                );
            }
            
        if(token.getLexicon().equals("this")) return new TypeRef(classTem.getName(),"ref");

        AttributeEntry atributeTem = classTem.variablesTable.get(token.getLexicon());
        
        

        if( atributeTem != null){
            if(atributeTem.getVisibility().equals("private")){
                HandleExceptionsSemantic(new InvalidVisibility(token.getLineNum(), token.getColNum(), token.getLexicon()));
            }else{
                // return new TypeRef(classTem.getName(),"void");
                return atributeTem.getTipo();
            }
            
        }
        HandleExceptionsSemantic(new TypeNotFound(token.getLineNum(), token.getColNum(), token.getLexicon()));
        return null;
    }

    /**
     * Busca el tipo de un metodo en su respectiva intancia
     * @param pClass clase padre.
     * @param pMethod metodo padre.
     * @param token token actale.
     * @return typo .
     */

    public Type searchTypeEn(String pClass,String pMethod, Token token)
    {
        ClassEntry classTem= classes.get(pClass);
        MethodEntry metodTem = classTem.methodsTable.get(pMethod);
        if( metodTem != null){
            return metodTem.mType;
        }
        HandleExceptionsSemantic(new TypeNotFound(token.getLineNum(), token.getColNum(), token.getLexicon()));
        return null;
    }
    /**
     * Busca el tipo de los argumentos de un metodo en una instancia
     * @param pClass clase padre.
     * @param pMethod metodo padre.
     */
    public Type searchTypeArg(String pClass,String pMethod, Integer cant)
    {
        int count = 0;
        ClassEntry classTem = classes.get(pClass);
        MethodEntry metodTem = classTem.methodsTable.get(pMethod);

        Type aux = null;
        for (VariableEntry iter : metodTem.parametersList.values()) 
        {
            aux =iter.getTipo();
            if(count==cant){
                break;
            }
            cant++;
        }
        return aux;
    }
    public Type searchTypeArgSobre(String pClass,String pMethod, Integer cant,MethodEntry met)
    {
        int count = 0;
        MethodEntry metodTem = met;
        Type aux = null;
        for (VariableEntry iter : metodTem.parametersList.values()) 
        {
            aux =iter.getTipo();
            if(count==cant){
                break;
            }
            cant++;
        }
        return aux;
    }
    /**
     * Busca la cantidad de argumentos de un metodo en una instancia
     * @param pClass clase padre.
     * @param pMethod metodo padre.
     * @return int .
     */
    public int searchTypeArgNum(String pClass,String pMethod)
    {
        ClassEntry classTem= classes.get(pClass);
        MethodEntry metodTem = classTem.methodsTable.get(pMethod);
        if( metodTem == null){
            return 0;
        }
        if(metodTem.overload){
            return -2;
        }

        int parMetTem = metodTem.parametersList.size();
        return parMetTem;
    }
    public MethodEntry[] searchTypeArgSobNum(String pClass,String pMethod)
    {
        ClassEntry classTem= classes.get(pClass);
        MethodEntry metodTem = classTem.methodsTable.get(pMethod);
        MethodEntry[] sobreMet = metodTem.variants;
        return sobreMet;
    }

    public void HandleExceptionsSemantic(SemanticException e) {
        throw e;
    }
}