package tinyjava.AnalizadorSemantico.TablaSimbolos;

import java.util.ArrayList;

import tinyjava.AnalizadorSemantico.Types.*;

/**
 * Representa una entrada de variable.
 */
public class VariableEntry extends TableEntry{
    private Type type; /*<Tipo de la variable. */
    public VariableEntry(String name,int row,int column,SymbolTable table,Type type) 
    {
        super(name,row,column,table);
        this.type = type;
    }

    public Type getTipo(){
        return this.type;
    } 

    
    @Override
    public ArrayList<String> printEntry(ArrayList<String> printListEDT) 
    {
        return printVariable(printListEDT);
    }


    public ArrayList<String> printVariable(ArrayList<String> printListEDT) {
        printListEDT.add("                "+type.getInstanceName()+" "+this.getName());
        return printListEDT;
    }
    public ArrayList<String> printVariable(ArrayList<String> printListEDT,int scp) {
        printListEDT.add("                "+type.getInstanceName()+" "+this.getName()+"(Scope "+Integer.toString(scp)+")");
        return printListEDT;
    }
}