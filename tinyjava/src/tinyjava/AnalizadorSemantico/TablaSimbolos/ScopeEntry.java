package tinyjava.AnalizadorSemantico.TablaSimbolos;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;

/**
 * Esta clase maneja un conjunto de scopes locales.
 */
public class ScopeEntry {
    private LinkedList<LocalScope> scopes; // lista de scopes
    private int currentPos = 0; // nombre del scope actual

    public ScopeEntry() {
        scopes = new LinkedList<LocalScope>();
        scopes.add(new LocalScope(currentPos));
    }

    /**
     * Añade una variable al scope actual.
     */
    public void addVariableToScope(VariableEntry vEntry) {
        scopes.get(currentPos).addVariable(vEntry);
    }

    /**
     * Checkea si una varialbe existe en el scope actual
     */
    public boolean variableExistsInCurrentScope(String name) {
        return scopes.get(currentPos).variableExists(name);
    }

    /**
     * Checkea si una variable existe en el scope ingresado.
     */
    public boolean variableExistsInScope(String name, int scope) {
        return getVariableFromScope(name, scope) != null;
    }

    /**
     * Crea un nuevo scope a partir del scope actual
     */
    public void branchScope() {
        LocalScope prev = scopes.get(currentPos);
        currentPos = scopes.size();
        scopes.add(new LocalScope(prev, currentPos));
    }

    /**
     * Retorna el nombre de scope actual
     */
    public int getCurrentScope() {
        return currentPos;
    }

    /**
     * Retorna al scope ingresado.
     */
    public void returnToScope(int pos) {
        if (pos < 0 || pos >= scopeSize()) {
            try {
                throw new Exception("Error. El scope ingresado no es válido.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        currentPos = pos;
    }

    @Deprecated
    public void printScope()
    {
        for (LocalScope localScope : scopes) {
            localScope.printScope();
        }
    }

    /**
     * Imprime el scope completo
     */
    public ArrayList<String> printScopeAll(ArrayList<String> printListEDT)
    {
        for (LocalScope scp : scopes) {
            printListEDT = scp.printScopeAll(printListEDT);
        }
        return printListEDT;
    }
 
    /**
     * Busca una variable de un scope ingresado y la retorna.
     */
    public VariableEntry getVariableFromScope(String name, int scope)
    {
        return scopes.get(scope).getVariable(name);
    }

    /**
     * Cantidad de scopes.
     */
    public int scopeSize()
    {
        return scopes.size();        
    }
}