package tinyjava.AnalizadorSemantico.TablaSimbolos;

import java.util.ArrayList;

import tinyjava.AnalizadorSemantico.Types.*;

/**
 * Esta clase permite generar override para el metodo print de la clase System.
 */
public class SystemMethodEntry extends MethodEntry 
{

    public SystemMethodEntry(SymbolTable table) {
        super("print", 0, 0, table, "static", new TypeVoid());
        overload = true;
        variants = new MethodEntry[4];
        addVariants();  
    }
    
    /**
     * Añade las variantes de print a un array
     */
    private void addVariants()
    {
        //print
        MethodEntry printb = new MethodEntry("print", 0, 0, this.symbolTable, "static", new TypeVoid());
        printb.addParameter(new VariableEntry("b", 0, 0, this.symbolTable, new TypeBoolean("bool")));
        variants[0] = printb;

        MethodEntry printc = new MethodEntry("print", 0, 0, this.symbolTable, "static", new TypeVoid());
        printc.addParameter(new VariableEntry("c", 0, 0, this.symbolTable, new TypeChar("char")));
        variants[1] = printc;

        MethodEntry printi = new MethodEntry("print", 0, 0, this.symbolTable, "static", new TypeVoid());
        printi.addParameter(new VariableEntry("i", 0, 0, this.symbolTable, new TypeInt("int")));
        variants[2] = printi;

        MethodEntry printS = new MethodEntry("print", 0, 0, this.symbolTable, "static", new TypeVoid());
        printS.addParameter(new VariableEntry("s", 0, 0, this.symbolTable, new TypeString("string")));
        variants[3] = printS;
        
    }

    @Override
    public ArrayList<String> printEntry(ArrayList<String> printListEDT) {
        for (MethodEntry methodEntry : variants) 
        {
            printListEDT.add("        < Metodo "+methodEntry.getName()+" >");
            printListEDT.add("            Forma: "+methodEntry.mForm);
            printListEDT.add("            Retorna: "+methodEntry.mType.getInstanceName());

            if(methodEntry.parametersList.size() != 0)
            {
                printListEDT.add("            Parametros: ");
                for (VariableEntry iter : methodEntry.parametersList.values()) 
                {
                    printListEDT=iter.printVariable(printListEDT);
                }
            }       
        }
        return printListEDT;
    }
}