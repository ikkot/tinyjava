package tinyjava.AnalizadorSemantico.TablaSimbolos;

import java.util.ArrayList;

/**
 * Representa el tipo de dato que se almancena en la tabla de símbolos.
 * Otras clases heredan de esta para crear metodos, clases y variables.
 */
public abstract class TableEntry 
{
    private String name;
    private int lineNumber;
    private int colNumber;
    SymbolTable symbolTable;
    public TableEntry(String name,int lineNumber,int colNumber,SymbolTable symbolTable)
    {
        this.name = name;
        this.lineNumber = lineNumber;
        this.colNumber = colNumber;
        this.symbolTable = symbolTable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public int getColNumber() {
        return colNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.colNumber = rowNumber;
    }

    public SymbolTable getSymbolTable() {
        return symbolTable;
    }

    public void setSymbolTable(SymbolTable symbolTable) {
        this.symbolTable = symbolTable;
    }

    public abstract ArrayList<String> printEntry(ArrayList<String> printListEDT);
}