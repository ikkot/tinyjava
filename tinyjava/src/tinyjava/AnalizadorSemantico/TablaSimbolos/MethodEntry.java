package tinyjava.AnalizadorSemantico.TablaSimbolos;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import tinyjava.AnalizadorSemantico.AST.Sentences.BlockNode;
import tinyjava.AnalizadorSemantico.Exceptions.NonExistentType;
import tinyjava.AnalizadorSemantico.Exceptions.NonReturn;
import tinyjava.AnalizadorSemantico.Exceptions.NonReturnVoid;
import tinyjava.AnalizadorSemantico.Exceptions.SemanticException;

// import org.graalvm.compiler.lir.Variable;

import tinyjava.AnalizadorSemantico.Types.Type;

public class MethodEntry extends TableEntry
{
    LinkedHashMap<String,VariableEntry> parametersList; //parametros del metodo
    // @Deprecated
    // LinkedHashMap<String,VariableEntry> localVarList; //variables declaradas localmente
    String mForm; //forma del metodo. Static|nostatic
    Type mType; //tipo del metodo. void|primitv|ref
    BlockNode block;
    MethodEntry[] variants; 
    Boolean overload = false;
    ScopeEntry scopes;
    
    
    public MethodEntry(String name, int row,int column,SymbolTable table,String form,Type tipo) 
    {
        super(name,row,column,table);
        mForm = form;
        mType = tipo;
        parametersList = new LinkedHashMap<String,VariableEntry>();
        // localVarList = new LinkedHashMap<String,VariableEntry>();
        scopes = new ScopeEntry();
    }

    //#region scopes

    public int getCurrentScope()
    {
        return scopes.getCurrentScope();
    }

    public void branchScope()
    {
        scopes.branchScope();
    }

    public void returnToScope(int i)
    {
        scopes.returnToScope(i);
    }
    //#endregion

    public void addParameter(VariableEntry vEntry)
    {
        parametersList.put(vEntry.getName(), vEntry);
    }
    public int addParameterNum()
    {
        return this.parametersList.size();
    }


    public void addLocalVar(VariableEntry vEntry)
    {
        // localVarList.put(vEntry.getName(), vEntry);
        scopes.addVariableToScope(vEntry);
    }

    /**
     * Retorna true si el parámetro ingresado existe. Falso en caso contrario.
     */
    public boolean ParameterExists(String v)
    {
        return parametersList.containsKey(v);
    }
    /**
     * Retorna true si la variable local ingresada existe. Falso en caso contrario.
     */
    // @Deprecated
    // public boolean LocalVarExists(String v)
    // {
    //     return localVarList.containsKey(v);
    // }

    /**
     * Busca una variable entre los parametros o las variables locales
     */
    public boolean VariableExists(String v)
    {
        // return ParameterExists(v) || LocalVarExists(v);
        return scopes.variableExistsInCurrentScope(v) || ParameterExists(v);
    }

    public boolean variableExistsInScope(String name,int scope)
    {
        return scopes.variableExistsInScope(name, scope);
    }

    public VariableEntry getVariableFromScope(String name, int scope)
    {
        return scopes.getVariableFromScope(name, scope);
    }

    public Type getTipo()
    {
        return mType;
    } 
    
    public LinkedHashMap<String,VariableEntry> getParametersList(){
        return parametersList;
    } 

    @Override
    public ArrayList<String> printEntry(ArrayList<String> printListEDT) 
    {
        return printMethod(printListEDT);
    }

    public void setBlock(BlockNode block)
    {
        this.block = block;
    }

    public ArrayList<String> printMethod(ArrayList<String> printListEDT) 
    {
        printListEDT.add("        < Metodo "+this.getName()+" >");
        printListEDT.add("            Forma: "+mForm);
        printListEDT.add("            Retorna: "+mType.getInstanceName());

        if(parametersList.size() != 0)
        {
            printListEDT.add("            Parametros: ");
            for (VariableEntry iter : parametersList.values()) 
            {
                printListEDT = iter.printVariable(printListEDT);
            }
        }
        
        if(scopes.scopeSize() != 1)
        {
            printListEDT.add("            Variables locales:");

            printListEDT = scopes.printScopeAll(printListEDT);
            // for (VariableEntry iter : localVarList.values()) 
            // {
            //     printListEDT = iter.printVariable(printListEDT);
            // }
        }

        //TODO: borrar, es para testeo
        // printScope();


        return printListEDT;
    }

    @Deprecated
    private void printScope()
    {
        scopes.printScope();
    }

    public void checkMethod(ClassEntry c)
    {
        if(block != null){
            this.symbolTable.returnState =false;
            block.checkNode(this.symbolTable,c.getName(),this.getName());
        }
        if(this.symbolTable.returnState && mType.getTypeName().equals("void")){
            HandleExceptions(new NonReturnVoid(this.getLineNumber(), this.getColNumber(),this.getName()));
        }
        if(!this.symbolTable.returnState && !(mType.getTypeName().equals("void"))){
            HandleExceptions(new NonReturn(this.getLineNumber(), this.getColNumber(),this.getName()));
        }
    }

    public ArrayList<String> checkMethodPrint(ArrayList<String> printListAST)
    {
        if(block != null){
            // for (VariableEntry iter : localVarList.values()) 
            // {
            //     printListAST.add("var: "+iter.getName()+" tipo: "+ iter.getTipo().getTypeName());
            // }
            // printListAST.add("");
            printListAST.add("--------------------------- AST  ---------------------------");
            printListAST.add("");
            printListAST = block.printAll(printListAST, "");}
        return printListAST;
    }

    // public VariableEntry getLocalVariable(String name)
    // {
    //     return localVarList.get(name);
    // }

    public VariableEntry getParameter(String name)
    {
        return parametersList.get(name);
    }

    public boolean isStatic()
    {
        return mForm.equals("static");
    }
    private void HandleExceptions(SemanticException e)
	{
		throw e;
    }
}