package tinyjava.AnalizadorSemantico.Exceptions;

public class RepeatedParameter extends SemanticException {

	public RepeatedParameter(int fila, int colum, String nameParameter, String method) {
		super("Método "+ method+": El parametro "+nameParameter+" ya fue declarado. ", fila, colum);
	}

}