package tinyjava.AnalizadorSemantico.Exceptions;

public class RepeatedAttrException extends SemanticException{
    public RepeatedAttrException(int fila, int colum, String nameParameter, String cls) 
    {
		super("Clase "+cls+ ": El atributo "+nameParameter+" ya ha sido declarado. ", fila, colum);
	}

}