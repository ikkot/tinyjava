package tinyjava.AnalizadorSemantico.Exceptions;

public class CircularInheritance extends SemanticException {

    public CircularInheritance(int fila, int colum, String nameClass) {
		super("La clase "+nameClass+" tiene en su linea de ancestros a la clase "+nameClass, fila, colum);
	}
}