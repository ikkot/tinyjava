package tinyjava.AnalizadorSemantico.Exceptions;

public class ArgsNumInvalid extends SemanticException {
	public ArgsNumInvalid(int fila, int colum, String var) {
		super(" El metodo "+ var+" tiene una diferente cantidad de argumentos " , fila,
				colum);
	}
}
