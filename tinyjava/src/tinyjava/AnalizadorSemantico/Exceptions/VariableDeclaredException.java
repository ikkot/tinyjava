package tinyjava.AnalizadorSemantico.Exceptions;

public class VariableDeclaredException extends SemanticException {
    public VariableDeclaredException(int fila,int colum,String vName)
    {
        super("La variable "+vName+ " ya ha sido declarada.", fila, colum);
    }
}