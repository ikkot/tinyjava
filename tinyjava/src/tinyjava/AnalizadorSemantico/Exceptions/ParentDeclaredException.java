package tinyjava.AnalizadorSemantico.Exceptions;

public class ParentDeclaredException extends SemanticException
{
    public ParentDeclaredException(int fila, int colum, String parent)
    {
        super("La clase padre \""+parent+"\" no existe.", fila, colum);
    }    
}