package tinyjava.AnalizadorSemantico.Exceptions;

public class NonExistentType extends SemanticException {

    public NonExistentType(int fila, int colum, String nameTipo) {
		super("El tipo de "+nameTipo+" no esta declarado ", fila, colum);
	}
}