package tinyjava.AnalizadorSemantico.Exceptions;

public class NonReturnVoid extends SemanticException {

    public NonReturnVoid(int fila, int colum, String nameTipo) {
		super("Esta funcion es de tipo void", fila, colum);
	}
}