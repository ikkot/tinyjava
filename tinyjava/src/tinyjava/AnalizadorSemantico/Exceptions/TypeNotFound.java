package tinyjava.AnalizadorSemantico.Exceptions;

public class TypeNotFound extends SemanticException {
	public TypeNotFound(int fila, int colum, String var) {
		super("No se declaro: " + var , fila,
				colum);
	}
}
