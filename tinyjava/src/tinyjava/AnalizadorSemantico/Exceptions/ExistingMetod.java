package tinyjava.AnalizadorSemantico.Exceptions;

public class ExistingMetod extends SemanticException {

	public ExistingMetod(int fila, int colum, String nameMetod) {
		super("El método "+nameMetod+" ya fue declarado.", fila, colum);
	}
}