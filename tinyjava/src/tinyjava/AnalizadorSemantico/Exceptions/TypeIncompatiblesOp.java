package tinyjava.AnalizadorSemantico.Exceptions;

public class TypeIncompatiblesOp extends SemanticException {
	public TypeIncompatiblesOp(int fila, int colum, String leftType, String rightType, String op) {
		super("El operador " + op + " no es compatible con los siguientes tipos: " + leftType + " y " + rightType, fila,
				colum);
	}
}
