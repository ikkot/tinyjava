package tinyjava.AnalizadorSemantico.Exceptions;

public class TypeIncompatibles extends SemanticException {
    public TypeIncompatibles(int fila, int colum, String leftType,String rightType) {
		super("Tipos incompatibles: "+leftType+" y "+rightType, fila, colum);
	}
}
