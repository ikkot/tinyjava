package tinyjava.AnalizadorSemantico.Exceptions;

public class ExistingClass extends SemanticException {

	public ExistingClass(int fila, int colum, String nameClass) {
		super("La clase "+nameClass+" ya está declarada.", fila, colum);
	}

}