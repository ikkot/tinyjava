package tinyjava.AnalizadorSemantico.Exceptions;

public class SemanticException extends RuntimeException {

    public SemanticException(String msg, int fila, int colum) {
        super("Error: " + msg + " (Ln " + Integer.toString(fila) + ":Col " + Integer.toString(colum) + ")");
    }

}