package tinyjava.AnalizadorSemantico.Exceptions;

public class CtorExistsException extends SemanticException
{
    public CtorExistsException(int fila,int colum,String name)
    {
        super("El constructor " +name+" ya fue declarado.", fila, colum);
    }    
}