package tinyjava.AnalizadorSemantico.Exceptions;

public class TypeIncompatiblesCon extends SemanticException {
    public TypeIncompatiblesCon(int fila, int colum, String con) {
		super("La condicion espera un boolean y recibe un: "+con, fila, colum);
	}
}
