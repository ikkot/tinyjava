package tinyjava.AnalizadorSemantico.Exceptions;

public class NonReturn extends SemanticException {

    public NonReturn(int fila, int colum, String nameTipo) {
		super("El metodo "+nameTipo+" siempre tiene que retornar un valor ", fila, colum);
	}
}