package tinyjava.AnalizadorSemantico.Exceptions;

public class SignatureException extends SemanticException
{
    public SignatureException(int fila,int colum,String name)
    {
        super("El metodo heredado " +name+" tiene diferente signatura que el metodo local.", fila, colum);
    }    
}