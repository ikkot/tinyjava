package tinyjava.AnalizadorSemantico.Exceptions;

public class InvalidVisibility extends SemanticException {
	public InvalidVisibility(int fila, int colum, String var) {
		super("La variable "+var +" que se quiere acceder no es publica " , fila,
				colum);
	}
}
