package tinyjava.AnalizadorSemantico.Exceptions;

public class NonExistingClass extends SemanticException {

	public NonExistingClass(int fila, int colum, String nameClass) {
		super("La clase referenciada  "+nameClass+" no existe", fila, colum);
	}

}