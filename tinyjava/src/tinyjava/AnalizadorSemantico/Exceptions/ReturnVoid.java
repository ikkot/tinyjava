package tinyjava.AnalizadorSemantico.Exceptions;

public class ReturnVoid extends SemanticException {
    public ReturnVoid(int fila, int colum) {
		super("No se puede retornar void. ", fila, colum);
	}
}
