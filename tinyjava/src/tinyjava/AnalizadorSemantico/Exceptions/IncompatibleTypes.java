package tinyjava.AnalizadorSemantico.Exceptions;

public class IncompatibleTypes extends SemanticException {

	public IncompatibleTypes(int fila, int colum, String nameParameter) {
		super("Tipos incompatibles ", fila, colum);
	}
}