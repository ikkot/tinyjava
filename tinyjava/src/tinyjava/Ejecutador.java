package tinyjava;
import tinyjava.AnalizadorLexico.*;
import tinyjava.AnalizadorSemantico.TablaSimbolos.SymbolTable;
import tinyjava.AnalizadorSintactico.*;
import java.util.ArrayList;


import java.io.FileWriter;
import java.io.IOException;

/**
 * Imprime un lista de los Token leidos por el LexicalAnalyzer junto con su lexema y pocicion
 * En caso de encontrar un Token no valido imprimer el error, donde se prodijo y para la ejecucion del programa
 */
public class Ejecutador {
    private ArrayList<String> printListToken;  /**< Lista de tokens validos para imprimir */
    private String path;  /**< Dirección del archivo leído. */
    private static Ejecutador ejec;
    private ArrayList<String> printListEDT;
    private ArrayList<String> printListAST;

    public static Ejecutador getInstance(){
        if (ejec == null){
            ejec = new Ejecutador();
        }
        return ejec;
    }

    public Ejecutador() { 
    }

     /**
     * Pide un Token al LexicalAnalyzer 
     * Lo almacena en una lista
     * En caso de encontrar Token con un error se imprime el error correspondiente y se detiene la  ejecucion
     * @return void .
     */
    public void requestAllToken(String path){
        printListToken = new ArrayList<String>();
        LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(path);
        this.path=path;

        printListToken.add("|    Token         ---       Lexicon        --  Line    --  Col  |");
        while (true) {
            try {
                Token aux = lexicalAnalyzer.NextToken();
                if (aux == null) {
                    break;
                } else {
                    int numTokenNormalize = 14 - aux.getToken().length();
                    int numLexiconNormalize = 15 - aux.getLexicon().length();
                    int numLineNormalize = 3 - String.valueOf(aux.getLineNum()).length();
                    int numColNormalize = 3 - String.valueOf(aux.getColNum()).length();
                    String tokenNormalizado = aux.getToken();
                    String lexiconNormalizado = aux.getLexicon();
                    String lineNormalizado = String.valueOf(aux.getLineNum());
                    String colNormalizado = String.valueOf(aux.getColNum());
                    int i;
                    for (i = 0; i <= numTokenNormalize; i++) {
                        tokenNormalizado = tokenNormalizado + " ";
                    }
                    for (i = 0; i <= numLexiconNormalize; i++) {
                        lexiconNormalizado = lexiconNormalizado + " ";
                    }
                    for (i = 0; i <= numLineNormalize; i++) {
                        lineNormalizado = lineNormalizado + " ";
                    }
                    for (i = 0; i <= numColNormalize; i++) {
                        colNormalizado = colNormalizado + " ";
                    }
                    printListToken.add("| " + tokenNormalizado + "  ---    " + lexiconNormalizado+"  --   "+lineNormalizado+"   --   "+colNormalizado+"|");
                }
            } catch (Exception e) {
                printListToken.add(" ");
                printListToken.add("Error encontrado: ");
                printListToken.add(e.toString());
                break;
            }
        } 
        
        
    }

    public Boolean ejecSintactico(String path,String fileName1, String fileName2){
        printListToken = new ArrayList<String>();
        //requestAllToken(path);
        //printTokenList();

        LexicalAnalyzer lexicalAn = new LexicalAnalyzer(path);
        SyntacticAnalyzer synSor = new SyntacticAnalyzer(lexicalAn);
        System.out.println("Analizando");

        try {

            SymbolTable table = synSor.start();
            printASTEDT(table,fileName1,fileName2);

            System.out.println("No se encontraron errores lexicos");
            System.out.println("No se encontraron errores sintacticos");
            System.out.println("No se encontraron errores semanticos");
            return true;
            
        } catch (Exception e) {
            System.out.println("Error encontrado: ");
            System.out.println(e.toString());
            return false;
        }       
    }

    /**
     * Imprime todos los Tokens con sus lexemas y ubicacion 
     * @return void .
     */
    public void printTokenList() {
        for (int i = 0; i < printListToken.size(); i++) {
            System.out.println(printListToken.get(i));
          }
    }

    /**
     * Crear archivo con todos los Tokens con sus lexemas y ubicacion.
     * @return void .
     */
    public void createFil() {
        try {
            String[] partialPath = path.split(".tj");
            FileWriter fw = new FileWriter(partialPath[0]+"_Output.txt");
            for (int i = 0; i < printListToken.size(); i++) {
                fw.write(printListToken.get(i)+"\n");
              }
            fw.close();
            System.out.println("Archivo creado en: "+partialPath[0]+"_Output.txt");
        } catch (IOException e) {
            System.err.println("Error al crear archivo");
            System.err.println(e);
        }
    }
    public void createFile(String name,ArrayList<String> list) {
        try {
            FileWriter fw = new FileWriter(name+".txt");
            for (int i = 0; i < list.size(); i++) {
                fw.write(list.get(i)+"\n");
              }
            fw.close();
            System.out.println("Archivo creado en: "+name+".txt");
        } catch (IOException e) {
            System.err.println("Error al crear archivo");
            System.err.println(e);
        }
    }
    public void createFilPer(String path) {
        try {
            FileWriter fw = new FileWriter(path);
            for (int i = 0; i < printListToken.size(); i++) {
                fw.write(printListToken.get(i)+"\n");
              }
            fw.close();
            System.out.println("Archivo creado en: "+path);
        } catch (IOException e) {
            System.err.println("Error al crear archivo");
            System.err.println(e);
        }
    }

    private void printASTEDT(SymbolTable table,String fileName1,String fileName2){
        printEDT( table,fileName1);
        printAST( table,fileName2);
    
    }

    private  void printEDT(SymbolTable table, String name){
        ArrayList<String> printListEDTTem = new ArrayList<String>();
        table.printClasses(printListEDTTem);
        Ejecutador.getInstance().createFile(name, printListEDTTem);
        this.printListEDT = printListEDTTem;

    }

    private  void printAST(SymbolTable table, String name){
        ArrayList<String> printListASTTem = new ArrayList<String>();
        printListASTTem= table.printAST(printListASTTem);
        Ejecutador.getInstance().createFile(name, printListASTTem);
        this.printListAST = printListASTTem;
    }
    public void printScreen(){
        String aux = "";
        for(int i=0;i<=100;i++){
            aux+="#";
        }
        String aux2 = "";
        for(int i=0;i<=47;i++){
            aux2+="#";
        }
        System.out.println("");
        System.out.println(aux);
        System.out.println(aux2+" EDT "+aux2);
        System.out.println(aux);
        System.out.println("");
        printList(this.printListEDT);

        System.out.println("");
        System.out.println(aux);
        System.out.println(aux2+" AST "+aux2);
        System.out.println(aux);

        printList(this.printListAST);
    }

    private void printList(ArrayList<String>  list){
        for (String line : list) {
            System.out.println(line);
        }
    }
}