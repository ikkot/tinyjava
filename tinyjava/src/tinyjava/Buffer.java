package tinyjava;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

//import jdk.javadoc.internal.doclets.formats.html.resources.standard_ja;

/**
 * Lee un archivo de entrada y retorna sus caracteres.
 */
public class Buffer {
    private String path; /**< Dirección del archivo leído. */
    private FileReader fileReader; /**< Objeto de java utilizado para leer archivos. */
    private String currentChar = "-2"; // -2 error al leer , -1 find del archivo (EOF)
    private int line = 1; /**< */
    private int col = 0;
    private Boolean consume = true;
    private String oldCurrentChar;
    public boolean EOF;  /**< true si ya se leyó todo el archivo. */
    private static Buffer buf;


    public static Buffer getInstance(){
        if (buf == null){
            buf = new Buffer();
        }
        return buf;
    }

    public Buffer() {
        this.EOF = false; 
    }

    /**
     * Abre un archivo en la ruta path correspondiente, este queda abierto hasta ejecutar closeFile()
     * En caso de encontrar un error lo imprime en pantalla y termina la ejecucion del programa
     * @return void .
     */
    public void openFile(String path) {
        try {
            this.EOF = false; 
            this.path = path;
            this.line = 1;
            this.col = 0;
            consume= true;
            fileReader = new FileReader(path);
        } catch (IOException e) {
            System.err.println("Erorr open");
            System.err.println(e);
            System.exit(-1);
        }
    }

    
    /**
     * Retorna el proximo caracter del archivo leido consumiendo el caracter.
     * Los caracteres se leen de arriba hacia abajo y de izquierda a derecha.
     * @return Caracter leído.
     */
    public String nexChar() {
        if (consume){
            try {
                int caracterLeido = fileReader.read();
                if (caracterLeido == -1) {// Fin del archivo
                    fileReader.close();
                    currentChar = "EOF";
                    EOF = true;
                } 
                else 
                {
                    // 13 por /r 10 por /n por estar en windows en ASCII
                    if (caracterLeido == 13) // Window
                    {
                        //caracterLeido = fileReader.read();
                        currentChar = "line_break";
                        //line++;
                        //col = 0;
                    }  else if (caracterLeido == 10 ) { // Linux
                        currentChar = "line_break";
                        line++;
                        col = 0;
                    }
                    else if (caracterLeido == 32 || caracterLeido == 9) 
                    { // 32 espacio , 9 tab
                        if (caracterLeido == 9){
                            col = col +4;
                        }else{
                            col++;
                        }
                        currentChar = "space";

                    }
                    else if((char)caracterLeido == '"')
                    {
                        currentChar = "doubleQuot";
                    }
                    else if(caracterLeido == 92) // barra invertida "\"
                    {
                        currentChar = "\\";
                    }
                    else 
                    {
                        char caracter2 = (char) caracterLeido;
                        currentChar = Character.toString(caracter2);
                        col++;
                    }
                }
            } 
            catch (IOException e) 
            {
                System.err.println(e.getMessage());
                System.exit(-1);
                currentChar = "-2";
            }
        }
        else
        {
            consume = true ;
        }
        
        return currentChar;
    }

    /**
     * Retorna el ultimo caracter leido sin consumir el caracter.
     * Los caracteres se leen de arriba hacia abajo y de izquierda a derecha.
     * @return Caracter leído.
     */
    public String seeChar() {
        if (consume == false){
            return oldCurrentChar;
        }else{
            return currentChar;
        }
        
    }

    /**
     * Retorna el proximo caracter del archivo leido sin consumir el caracter. 
     * Los caracteres se leen de arriba hacia abajo y de izquierda a derecha.
     * @return Caracter leído.
     */
    public String seeWithoutConsume(){
        String aux;
        if (consume == true){
            oldCurrentChar = currentChar;
            aux = nexChar();
            consume = false;
        }else{
            aux = currentChar;
        }
        return aux;
    }

    /**
     * Cierra el archivo abierto
     */
    public void closeFile() {
        try {
            fileReader.close();
        } catch (IOException e) {
            System.err.println("Error close");
        }
    }

    public void createFil() {
        try {
            FileWriter fw = new FileWriter(path);
            fw.write("Hola");
            fw.close();
        } catch (IOException e) {
            System.err.println("Error al crear");
        }
    }
    
    public int GetLineNumber()
    {
    	return this.line;
    }
    public int GetColNumber()
    {
    	return this.col;
    }
}
