
import tinyjava.Ejecutador;
import tinyjava.Interface;
import java.util.Scanner;

/**
 * Clase principal encargada de comenzar la ejecucion, ejecutar la interfaz en
 * bucle
 */

class Main {
    public static void main(String[] args) {
        Integer aux = 1;
        if (aux == 0) {
            System.out.println("---- Sintactico ----");
            System.out.println("    -Para ejecutar imprimiendo en pantalla :java Main prueba.tj ");
            if (args.length == 1) {
                Ejecutador eje = Ejecutador.getInstance();
                if (args[0].contains(".tj")) {
                    //eje.ejecSintactico(args[0]);

                } else {
                    System.out.println("Archivo no compatible");
                }
            } else {
                System.out.println("Numero de parametros incorrecto");
            }
        }else if(aux == 1){
            System.out.println("");
            System.out.println("#########################################################################");
            System.out.println("----------------------------- TinyJava ----------------------------------");
            System.out.println("#########################################################################");
            System.out.println("");
            System.out.println("    -Para ejecutar imprimiendo en pantalla :java Main prueba.tj ");
            System.out.println("");
            Ejecutador eje = Ejecutador.getInstance();
            Boolean erAu;
            if (args.length <= 3) {
                switch(args.length) {
                    case 0:
                        String path = System.getProperty("user.dir")+"/Tests/test";
                        erAu =eje.ejecSintactico(path,"EDT_Output","AST_Output");
                        if(erAu){eje.printScreen();}
                        break;
                    case 1:
                        if (args[0].contains(".tj")) {
                            erAu =eje.ejecSintactico(args[0],"EDT_Output","AST_Output");
                            if(erAu){eje.printScreen();}
                        } else {
                            System.out.println("Archivo no compatible");
                        }
                        break;
                    case 2:
                        if (args[0].contains(".tj")) {
                            eje.ejecSintactico(args[0],args[1],"AST_Output");
                        } else {
                            System.out.println("Archivo no compatible");
                        }
                        break;
                    case 3:
                        if (args[0].contains(".tj")) {
                            eje.ejecSintactico(args[0],args[1],args[2]);
                        } else {
                            System.out.println("Archivo no compatible");
                        }
                        break;
                    default:
                        System.out.println("Archivo no compatible");
    
                  }
                

            } else {
                System.out.println("Numero de parametros incorrecto");
            }

        } else {
            System.out.println("---- Lexico ----");
            System.out.println("Tipos de ejecucion :");
            System.out.println("    -Para ejecutar con interfaz : java Main");
            System.out.println("    -Para ejecutar imprimiendo en pantalla :java Main prueba.tj ");
            System.out.println("    -Para ejecutar escribiendo archivo : java Main prueba.tj salida.txt ");

            if (args.length > 2) { // si hay más de 2 parámetro
                System.out.println("Hay demasiados parámetros.");
            } else if (args.length == 0) { // si no hay parámetros

                Scanner imputScanner = new Scanner(System.in); // Creación de un objeto Scanner
                String inputTecladoString = "";
                // String path = "tinyjava" + "/" + "Tests";// path principal de la carptea test
                String path = "Tests";
                System.out.println("");
                System.out.println("#########################################################################");
                System.out.println("------------------------------TinyJava-----------------------------------");
                System.out.println("#########################################################################");
                System.out.println("");

                System.out.println("Test Directory = " + System.getProperty("user.dir") + "/" + path);
                System.out.println("Para agregar un nuevo archivo de test, colocarlo en Test Directory");

                Interface inter = Interface.getInstance(path);

                while (true) {
                    String finalPath = inter.searchFile();
                    // System.out.print(finalPath);
                    try {
                        Ejecutador eje = Ejecutador.getInstance();
                        eje.requestAllToken(finalPath);
                        eje.printTokenList();
                        System.out.print("Desea crear archivo de salida (y/N): ");
                        inputTecladoString = imputScanner.nextLine();
                        if (inputTecladoString.equals("y") || inputTecladoString.equals("Y")) {
                            eje.createFil();
                            break;
                        }
                        System.out.print("Desea continuar (Y/n): ");
                        inputTecladoString = imputScanner.nextLine();
                        if (inputTecladoString.equals("n") || inputTecladoString.equals("N")) {
                            break;
                        }
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                }
                imputScanner.close();
            } else if (args.length == 2) {
                Ejecutador eje = Ejecutador.getInstance();
                if (args[0].contains(".tj")) {
                    eje.requestAllToken(args[0]);
                    eje.createFilPer(args[1]);
                } else {
                    System.out.println("Archivo no compatible");
                }

            } else {
                Ejecutador eje = Ejecutador.getInstance();
                if (args[0].contains(".tj")) {
                    eje.requestAllToken(args[0]);
                    eje.printTokenList();
                } else {
                    System.out.println("Archivo no compatible");
                }

            }
        }
    }
}