# TinyJava

## Archivo Final


### Para ejecutar .class

- Descomprima el rar correspondiente Tinyjava.class.rar
- Para ejecutar archivo de Test imprimiendo en pantalla : `java Main `
- Para ejecutar imprimiendo en pantalla  : `java Main prueba.tj`
- Para ejecutar creando archivo de salida personalizado del EDT : `java Main prueba.tj edt.txt`
- Para ejecutar creando archivo de salida personalizado del EDT y AST : `java Main prueba.tj edt.txt ast.txt`

### Para ejecutar .jar

- Cambiar 'java Main' por `java -cp Tinyjava.jar Main `
